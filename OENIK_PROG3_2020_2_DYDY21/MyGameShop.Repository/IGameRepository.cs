﻿// <copyright file="IGameRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Repository
{
    using MyGameShop.Data;

    /// <summary>
    /// Interface of game repository.
    /// </summary>
    public interface IGameRepository : IRepository<Game>
    {
        /// <summary>
        /// Changes the price of a game.
        /// </summary>
        /// <param name="entity"> ID to change.</param>
        /// <param name="newPrice"> Price to change to.</param>
        void Update(Game entity, int newPrice);
    }
}
