﻿// <copyright file="PlatformRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Repository
{
    using System;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using MyGameShop.Data;

    /// <summary>
    /// Class of platform repository.
    /// </summary>
    public class PlatformRepository : GenericRepository<Platform>, IPlatformRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlatformRepository"/> class.
        /// </summary>
        /// <param name="ctx">DB context.</param>
        public PlatformRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public override Platform GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.PlatformId == id);
        }

        /// <inheritdoc/>
        public void Update(Platform entity, int newVersion)
        {
            try
            {
                if (entity != null)
                {
                    entity.Version = newVersion;
                    this.ctx.SaveChanges();
                }
            }
            catch (ArgumentNullException e)
            {
                throw new ArgumentNullException("No game was given", e);
            }
        }
    }
}
