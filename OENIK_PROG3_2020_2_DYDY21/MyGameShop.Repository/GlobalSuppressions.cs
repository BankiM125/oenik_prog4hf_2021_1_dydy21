﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<Was allowed>", Scope = "member", Target = "~F:MyGameShop.Repository.GenericRepository`1.ctx")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "<Was allowed>", Scope = "member", Target = "~F:MyGameShop.Repository.GenericRepository`1.ctx")]
