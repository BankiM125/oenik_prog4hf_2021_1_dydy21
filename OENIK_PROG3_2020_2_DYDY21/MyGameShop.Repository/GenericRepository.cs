﻿// <copyright file="GenericRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Repository
{
    using System;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Generic repository class.
    /// </summary>
    /// <typeparam name="T">Generic class variable.</typeparam>
    public abstract class GenericRepository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Database context.
        /// </summary>
        protected DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{T}"/> class.
        /// </summary>
        /// <param name="ctx">Database context.</param>
        protected GenericRepository(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Creates an entity.
        /// </summary>
        /// <param name="entity">Entity to create.</param>
        public void Create(T entity)
        {
           this.ctx.Add(entity);
           this.ctx.SaveChanges();
        }

        /// <summary>
        /// Gets all the entities.
        /// </summary>
        /// <returns> A quearyable type.</returns>
        public IQueryable<T> GetAll()
        {
            return this.ctx.Set<T>();
        }

        /// <summary>
        /// Gets one entity.
        /// </summary>
        /// <param name="id">Id of entity.</param>
        /// <returns>Entity.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// Deletes an entity.
        /// </summary>
        /// <param name="entity">Entity to delete.</param>
        /// <returns>Whetehr it was successful.</returns>
        public bool Delete(T entity)
        {
            try
            {
                this.ctx.Remove(entity);
                this.ctx.SaveChanges();
                return true;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }
    }
}
