﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Repository
{
    using System.Linq;

    /// <summary>
    /// Generic repository interface.
    /// </summary>
    /// <typeparam name="T">Generic type of class.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Gets a T type.
        /// </summary>
        /// <param name="id">ID of class.</param>
        /// <returns>T class.</returns>
        T GetOne(int id);

        /// <summary>
        /// Gets all the T class entities.
        /// </summary>
        /// <returns>Queary of T.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Creates T entity.
        /// </summary>
        /// <param name="entity">Entity.</param>
        void Create(T entity);

        /// <summary>
        /// Deletes entity.
        /// </summary>
        /// <param name="entity">Entity.</param>
        /// <returns>Whether it was successful.</returns>
        bool Delete(T entity);
    }
}
