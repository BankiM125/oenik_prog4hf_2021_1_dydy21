﻿// <copyright file="PublisherRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Repository
{
    using System;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using MyGameShop.Data;

    /// <summary>
    /// Publisher repository class.
    /// </summary>
    public class PublisherRepository : GenericRepository<Publisher>, IPublisherRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PublisherRepository"/> class.
        /// </summary>
        /// <param name="ctx">DB context.</param>
        public PublisherRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public void Update(Publisher entity, string newCeo)
        {
            try
            {
                if (entity != null)
                {
                    entity.CEO = newCeo;
                    this.ctx.SaveChanges();
                }
            }
            catch (ArgumentNullException e)
            {
                throw new ArgumentNullException("No game was given", e);
            }
        }

        /// <inheritdoc/>
        public override Publisher GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.PublisherId == id);
        }
    }
}
