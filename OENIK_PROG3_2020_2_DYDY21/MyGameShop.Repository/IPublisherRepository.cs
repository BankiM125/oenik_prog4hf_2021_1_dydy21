﻿// <copyright file="IPublisherRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Repository
{
    using MyGameShop.Data;

    /// <summary>
    /// Interface for publisher repository.
    /// </summary>
    public interface IPublisherRepository : IRepository<Publisher>
    {
        /// <summary>
        /// Changes  the CEO of a publisher.
        /// </summary>
        /// <param name="entity"> ID to change.</param>
        /// <param name="newCeo"> Name to change to.</param>
        void Update(Publisher entity, string newCeo);
    }
}
