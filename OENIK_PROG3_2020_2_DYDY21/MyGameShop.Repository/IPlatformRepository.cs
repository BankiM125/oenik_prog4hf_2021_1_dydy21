﻿// <copyright file="IPlatformRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Repository
{
    using MyGameShop.Data;

    /// <summary>
    /// Interface for platform repository.
    /// </summary>
    public interface IPlatformRepository : IRepository<Platform>
    {
        /// <summary>
        /// Changes the version of a platform.
        /// </summary>
        /// <param name="entity">Id of platform.</param>
        /// <param name="newVersion">Version to change to.</param>
        void Update(Platform entity, int newVersion);
    }
}
