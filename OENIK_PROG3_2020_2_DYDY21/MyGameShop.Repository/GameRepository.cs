﻿// <copyright file="GameRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Repository
{
    using System;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using MyGameShop.Data;

    /// <summary>
    /// Class of game repository.
    /// </summary>
    public class GameRepository : GenericRepository<Game>, IGameRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameRepository"/> class.
        /// </summary>
        /// <param name="ctx"> Db context.</param>
        public GameRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Changes the price of a game.
        /// </summary>
        /// <param name="entity"> Id to change.</param>
        /// <param name="newPrice"> Price to change to.</param>
        public void Update(Game entity, int newPrice)
        {
            try
            {
                if (entity != null)
                {
                    entity.Price = newPrice;
                    this.ctx.SaveChanges();
                }
            }
            catch (ArgumentNullException e)
            {
                throw new ArgumentNullException("No game was given", e);
            }
        }

        /// <summary>
        /// Gets a game.
        /// </summary>
        /// <param name="id">Id to get.</param>
        /// <returns>Returns a game entity.</returns>
        public override Game GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id == id);
        }
    }
}