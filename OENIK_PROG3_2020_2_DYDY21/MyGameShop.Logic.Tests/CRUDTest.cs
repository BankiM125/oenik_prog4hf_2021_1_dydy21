﻿// <copyright file="CRUDTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
/*
namespace MyGameShop.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using MyGameShop.Data;
    using MyGameShop.Logic;
    using MyGameShop.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Tests class.
    /// </summary>
    [TestFixture]
    public class CRUDTest
    {
        private Mock<IGameRepository> mockedGameRepo;
        private Mock<IPublisherRepository> mockedPubRepo;
        private Mock<IPlatformRepository> mockedPlatRepo;
        private List<Game> games;
        private List<Publisher> publishers;
        private List<Platform> platforms;

        /// <summary>
        /// Sets up the required test methods.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockedGameRepo = new Mock<IGameRepository>(
            MockBehavior.Loose);

            this.mockedPubRepo = new Mock<IPublisherRepository>(
            MockBehavior.Loose);

            this.mockedPlatRepo = new Mock<IPlatformRepository>(
            MockBehavior.Loose);

            this.games = new List<Game>()
            {
                  new Game() { Id = 1, GameName = "Cyberpunk 2077", Price = 60, PublisherId = 1, Genre = "Action", Release = new DateTime(2020, 12, 05), PlatformId = 1 },
                  new Game() { Id = 2, GameName = "VALORANT", Price = 12, PublisherId = 2, Genre = "Shooter", Release = new DateTime(2020, 04, 10), PlatformId = 2 },
                  new Game() { Id = 3, GameName = "The Witcher 3", Price = 25, PublisherId = 1, Genre = "RPG", Release = new DateTime(2015, 05, 18), PlatformId = 3 },
                  new Game() { Id = 4, GameName = "Counter Strike: Global Offensive", Price = 20, PublisherId = 4, Genre = "Shooter", Release = new DateTime(2012, 07, 21), PlatformId = 1 },
                  new Game() { Id = 5, GameName = "The Elder Scrolls V: Skyrim", Price = 20, PublisherId = 3, Genre = "RPG", Release = new DateTime(2012, 06, 26), PlatformId = 4 },
            };

            this.publishers = new List<Publisher>()
            {
                  new Publisher() { PublisherId = 1, Name = "CDProjectRed", CEO = "Will Smith", Country = "Poland", DoesSales = true, TripleA = true },
                  new Publisher() { PublisherId = 2, Name = "Riot Games", CEO = "Will Smith", Country = "USA", DoesSales = false, TripleA = false },
                  new Publisher() { PublisherId = 3, Name = "Bethesda Softworks", CEO = "Will Smith", Country = "USA", DoesSales = false, TripleA = true },
                  new Publisher() { PublisherId = 4, Name = "Valve Corporation", CEO = "Will Smith", Country = "USA", DoesSales = false, TripleA = true },
                  new Publisher() { PublisherId = 5, Name = "EA Games", CEO = "Will Smith", Country = "Canada", DoesSales = true, TripleA = true },
            };

            this.platforms = new List<Platform>()
            {
                   new Platform() { PlatformId = 1, Name = "Windows 10", ShortName = "WIN10", Corporation = "Microsoft", PaidOnlineService = false, Version = 10, NumberOfUsers = 1000000 },
                   new Platform() { PlatformId = 2, Name = "XBOX Series x", ShortName = "XBOXX", Corporation = "Microsoft", PaidOnlineService = true, Version = 1, NumberOfUsers = 200000 },
                   new Platform() { PlatformId = 3, Name = "Playstation 5", ShortName = "PS5", Corporation = "Sony", PaidOnlineService = true, Version = 5, NumberOfUsers = 210000 },
                   new Platform() { PlatformId = 4, Name = "Nintendo Switch", ShortName = "Switch", Corporation = "Nintendo", PaidOnlineService = true, Version = 1, NumberOfUsers = 120000 },
                   new Platform() { PlatformId = 5, Name = "Android 11", ShortName = "Android", Corporation = "Google", PaidOnlineService = false, Version = 11, NumberOfUsers = 9500000 },
            };
        }

        /// <summary>
        /// Tests the Get All method.
        /// </summary>
        [Test]
        public void GetAllTest()
        {
            this.mockedGameRepo.Setup(repo => repo.GetAll()).Returns(this.games.AsQueryable());
            this.mockedPubRepo.Setup(repo => repo.GetAll()).Returns(this.publishers.AsQueryable());
            this.mockedPlatRepo.Setup(repo => repo.GetAll()).Returns(this.platforms.AsQueryable());

            ICustomerLogic<Game> logicGame = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            ICustomerLogic<Platform> logicPlatform = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            ICustomerLogic<Publisher> logicPublisher = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);

            var gameResult = logicGame.GetAll();
            var platformResult = logicPlatform.GetAll();
            var publisherResult = logicPublisher.GetAll();

            this.mockedGameRepo.Verify(x => x.GetAll());
            this.mockedPlatRepo.Verify(x => x.GetAll());
            this.mockedPubRepo.Verify(x => x.GetAll());
        }

        /// <summary>
        /// Tests the GetOne method.
        /// </summary>
        /// <param name="id">ID of the entity.</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void GetOneTest(int id)
        {
            this.mockedGameRepo.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns(this.games[id]);
            this.mockedPlatRepo.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns(this.platforms[id]);
            this.mockedPubRepo.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns(this.publishers[id]);

            ICustomerLogic<Game> logicGame = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            ICustomerLogic<Platform> logicPlatform = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            ICustomerLogic<Publisher> logicPublisher = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);

            var gameResult = logicGame.GetOne(id);
            var platformResult = logicPlatform.GetOne(id);
            var publisherResult = logicPublisher.GetOne(id);

            this.mockedGameRepo.Verify(x => x.GetOne(id));
            this.mockedPlatRepo.Verify(x => x.GetOne(id));
            this.mockedPubRepo.Verify(x => x.GetOne(id));
        }

        /// <summary>
        /// Tests the Create method.
        /// </summary>
        [Test]
        public void CreateTest()
        {
            IAdminLogic<Game> logicGame = new AdminLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            IAdminLogic<Platform> logicPlatform = new AdminLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            IAdminLogic<Publisher> logicPublisher = new AdminLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);

            this.mockedGameRepo.Setup(repo => repo.Create(It.IsAny<Game>())).Callback<Game>((Game g) => this.games.Add(g));
            this.mockedPubRepo.Setup(repo => repo.Create(It.IsAny<Publisher>())).Callback<Publisher>((Publisher g) => this.publishers.Add(g));
            this.mockedPlatRepo.Setup(repo => repo.Create(It.IsAny<Platform>())).Callback<Platform>((Platform g) => this.platforms.Remove(g));
            var game = new Game { GameName = "Cyberpunk 2077", Price = 60, PublisherId = 1, Genre = "Action", Release = new DateTime(2020, 12, 05), PlatformId = 1 };
            var platform = new Platform() { Name = "Windows 10", ShortName = "WIN10", Corporation = "Microsoft", PaidOnlineService = false, Version = 10, NumberOfUsers = 1000000 };
            var publisher = new Publisher() { Name = "Riot Games", CEO = "Will Smith", Country = "USA", DoesSales = false, TripleA = false };

            logicGame.Create(game);
            logicPlatform.Create(platform);
            logicPublisher.Create(publisher);

            this.mockedGameRepo.Verify(x => x.Create(game));
            this.mockedPlatRepo.Verify(x => x.Create(platform));
            this.mockedPubRepo.Verify(x => x.Create(publisher));
        }

        /// <summary>
        /// Tests the Delete method.
        /// </summary>
        [Test]
        public void DeleteTest()
        {
            IAdminLogic<Game> logicGame = new AdminLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            IAdminLogic<Platform> logicPlatform = new AdminLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            IAdminLogic<Publisher> logicPublisher = new AdminLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);

            this.mockedGameRepo.Setup(repo => repo.Delete(It.IsAny<Game>())).Returns(true);
            this.mockedPubRepo.Setup(repo => repo.Delete(It.IsAny<Publisher>())).Returns(true);
            this.mockedPlatRepo.Setup(repo => repo.Delete(It.IsAny<Platform>())).Returns(true);
            var game = new Game { Id = 1, GameName = "Cyberpunk 2077", Price = 60, PublisherId = 1, Genre = "Action", Release = new DateTime(2020, 12, 05), PlatformId = 1 };
            var platform = new Platform() { PlatformId = 1, Name = "Windows 10", ShortName = "WIN10", Corporation = "Microsoft", PaidOnlineService = false, Version = 10, NumberOfUsers = 1000000 };
            var publisher = new Publisher() { PublisherId = 2, Name = "Riot Games", CEO = "Will Smith", Country = "USA", DoesSales = false, TripleA = false };

            logicGame.Remove(game);
            logicPlatform.Remove(platform);
            logicPublisher.Remove(publisher);

            this.mockedGameRepo.Verify(x => x.Delete(game));
            this.mockedPlatRepo.Verify(x => x.Delete(platform));
            this.mockedPubRepo.Verify(x => x.Delete(publisher));
        }

        /// <summary>
        /// Test for the Update methods.
        /// </summary>
        /// <param name="id">Id of item to update.</param>
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void UpdateTest(int id)
        {
            IAdminLogic<Game> logicGame = new AdminLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            IAdminLogic<Platform> logicPlatform = new AdminLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            IAdminLogic<Publisher> logicPublisher = new AdminLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);

            this.mockedGameRepo.Setup(repo => repo.Update(It.IsAny<Game>(), It.IsAny<int>())).Callback(() => this.games[id].Price = 20);
            this.mockedPubRepo.Setup(repo => repo.Update(It.IsAny<Publisher>(), It.IsAny<string>())).Callback(() => this.publishers[id].CEO = "Teszt");
            this.mockedPlatRepo.Setup(repo => repo.Update(It.IsAny<Platform>(), It.IsAny<int>())).Callback(() => this.platforms[id].Version = 10);

            logicGame.Update(this.games[id], "20");
            logicPlatform.Update(this.platforms[id], "10");
            logicPublisher.Update(this.publishers[id], "Teszt");

            this.mockedGameRepo.Verify(x => x.Update(this.games[id], 20));
            this.mockedPlatRepo.Verify(x => x.Update(this.platforms[id], 10));
            this.mockedPubRepo.Verify(x => x.Update(this.publishers[id], "Teszt"));
        }
    }
}*/