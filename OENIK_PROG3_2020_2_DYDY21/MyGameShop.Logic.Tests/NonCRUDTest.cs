﻿// <copyright file="NonCRUDTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using MyGameShop.Data;
    using MyGameShop.Logic;
    using MyGameShop.Repository;
    using NUnit.Framework;

    /// <summary>
    /// NonCRUD method tests.
    /// </summary>
    [TestFixture]
    public class NonCRUDTest
    {
        private Mock<IGameRepository> mockedGameRepo;
        private Mock<IPublisherRepository> mockedPubRepo;
        private Mock<IPlatformRepository> mockedPlatRepo;
        private List<Game> games;
        private List<Publisher> publishers;
        private List<Platform> platforms;

        /// <summary>
        /// Sets up the required test methods.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockedGameRepo = new Mock<IGameRepository>(
            MockBehavior.Loose);

            this.mockedPubRepo = new Mock<IPublisherRepository>(
            MockBehavior.Loose);

            this.mockedPlatRepo = new Mock<IPlatformRepository>(
            MockBehavior.Loose);

            this.games = new List<Game>()
            {
                  new Game() { Id = 1, GameName = "Cyberpunk 2077", Price = 60, PublisherId = 1, Genre = "Action", Release = new DateTime(2020, 12, 05), PlatformId = 1 },
                  new Game() { Id = 2, GameName = "VALORANT", Price = 12, PublisherId = 2, Genre = "Shooter", Release = new DateTime(2020, 04, 10), PlatformId = 2 },
                  new Game() { Id = 3, GameName = "The Witcher 3", Price = 25, PublisherId = 1, Genre = "RPG", Release = new DateTime(2015, 05, 18), PlatformId = 3 },
                  new Game() { Id = 4, GameName = "Counter Strike: Global Offensive", Price = 20, PublisherId = 4, Genre = "Shooter", Release = new DateTime(2012, 07, 21), PlatformId = 1 },
                  new Game() { Id = 5, GameName = "The Elder Scrolls V: Skyrim", Price = 20, PublisherId = 3, Genre = "RPG", Release = new DateTime(2012, 06, 26), PlatformId = 4 },
            };

            this.publishers = new List<Publisher>()
            {
                  new Publisher() { PublisherId = 1, Name = "CDProjectRed", CEO = "Will Smith", Country = "Poland", DoesSales = true, TripleA = true },
                  new Publisher() { PublisherId = 2, Name = "Riot Games", CEO = "Will Smith", Country = "USA", DoesSales = false, TripleA = false },
                  new Publisher() { PublisherId = 3, Name = "Bethesda Softworks", CEO = "Will Smith", Country = "USA", DoesSales = false, TripleA = true },
                  new Publisher() { PublisherId = 4, Name = "Valve Corporation", CEO = "Will Smith", Country = "USA", DoesSales = false, TripleA = true },
                  new Publisher() { PublisherId = 5, Name = "EA Games", CEO = "Will Smith", Country = "Canada", DoesSales = true, TripleA = true },
            };

            this.platforms = new List<Platform>()
            {
                   new Platform() { PlatformId = 1, Name = "Windows 10", ShortName = "WIN10", Corporation = "Microsoft", PaidOnlineService = false, Version = 10, NumberOfUsers = 1000000 },
                   new Platform() { PlatformId = 2, Name = "XBOX Series x", ShortName = "XBOXX", Corporation = "Microsoft", PaidOnlineService = true, Version = 1, NumberOfUsers = 200000 },
                   new Platform() { PlatformId = 3, Name = "Playstation 5", ShortName = "PS5", Corporation = "Sony", PaidOnlineService = true, Version = 5, NumberOfUsers = 210000 },
                   new Platform() { PlatformId = 4, Name = "Nintendo Switch", ShortName = "Switch", Corporation = "Nintendo", PaidOnlineService = true, Version = 1, NumberOfUsers = 120000 },
                   new Platform() { PlatformId = 5, Name = "Android 11", ShortName = "Android", Corporation = "Google", PaidOnlineService = false, Version = 11, NumberOfUsers = 9500000 },
            };
        }

        /// <summary>
        /// Tests the method which lists the average prices per platform.
        /// </summary>
        [Test]
        public void AverageOnPlatformTest()
        {
            this.mockedGameRepo.Setup(repo => repo.GetAll()).Returns(this.games.AsQueryable());
            this.mockedPubRepo.Setup(repo => repo.GetAll()).Returns(this.publishers.AsQueryable());
            this.mockedPlatRepo.Setup(repo => repo.GetAll()).Returns(this.platforms.AsQueryable());

            ICustomerLogic<Game> logicGame = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            ICustomerLogic<Platform> logicPlatform = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            ICustomerLogic<Publisher> logicPublisher = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            CustomerLogic logic = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);

            IList<Average> expectedAverages = new List<Average>()
            {
                new Average() { Name = "XBOX Series x", Avg = 12 },
                new Average() { Name = "Nintendo Switch", Avg = 20 },
                new Average() { Name = "Playstation 5", Avg = 25 },
                new Average() { Name = "Windows 10", Avg = 40 },
            };

            var actualAverages = logic.AverageOnPlatform();

            Assert.That(actualAverages, Is.EquivalentTo(expectedAverages));
            this.mockedGameRepo.Verify(x => x.GetAll(), Times.AtLeastOnce);
            this.mockedPlatRepo.Verify(x => x.GetAll(), Times.AtLeastOnce);
            this.mockedPubRepo.Verify(x => x.GetAll(), Times.Never);
        }

        /// <summary>
        /// Tests the method which lists platforms and the games on them.
        /// </summary>
        [Test]
        public void GamesOnPlatformTest()
        {
            this.mockedGameRepo.Setup(repo => repo.GetAll()).Returns(this.games.AsQueryable());
            this.mockedPubRepo.Setup(repo => repo.GetAll()).Returns(this.publishers.AsQueryable());
            this.mockedPlatRepo.Setup(repo => repo.GetAll()).Returns(this.platforms.AsQueryable());

            ICustomerLogic<Game> logicGame = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            ICustomerLogic<Platform> logicPlatform = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            ICustomerLogic<Publisher> logicPublisher = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            CustomerLogic logic = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);

            var q = from platforms in this.platforms
                    join games in this.games on platforms.PlatformId
                    equals games.PlatformId
                    select new PlatformGame(platforms.Games) { Platform = platforms.Name, };

            var actualAverages = logic.GamesOnPlatform();

            Assert.That(actualAverages, Is.EquivalentTo(q.ToList()));
            this.mockedGameRepo.Verify(x => x.GetAll(), Times.AtLeastOnce);
            this.mockedPlatRepo.Verify(x => x.GetAll(), Times.AtLeastOnce);
            this.mockedPubRepo.Verify(x => x.GetAll(), Times.Never);
        }

        /// <summary>
        /// Tests the method which lists genres of the publishers' games.
        /// </summary>
        [Test]
        public void GetGenresOfPublishersTest()
        {
            this.mockedGameRepo.Setup(repo => repo.GetAll()).Returns(this.games.AsQueryable());
            this.mockedPubRepo.Setup(repo => repo.GetAll()).Returns(this.publishers.AsQueryable());
            this.mockedPlatRepo.Setup(repo => repo.GetAll()).Returns(this.platforms.AsQueryable());

            ICustomerLogic<Game> logicGame = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            ICustomerLogic<Platform> logicPlatform = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            ICustomerLogic<Publisher> logicPublisher = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);
            CustomerLogic logic = new CustomerLogic(this.mockedGameRepo.Object, this.mockedPlatRepo.Object, this.mockedPubRepo.Object);

            var q = from games in this.games
                    join publishers in this.publishers on games.PublisherId
                    equals publishers.PublisherId
                    select new GamesOfPubs(publishers.Games)
                    {
                        PublisherName = publishers.Name,
                    };

            var actualAverages = logic.GetGenresOfPublishers();

            Assert.That(actualAverages, Is.EquivalentTo(q.ToList()));
            this.mockedGameRepo.Verify(x => x.GetAll(), Times.AtLeastOnce);
            this.mockedPlatRepo.Verify(x => x.GetAll(), Times.Never);
            this.mockedPubRepo.Verify(x => x.GetAll(), Times.AtLeastOnce);
        }
    }
}
