﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.WPFAPI.BL
{
    using System;
    using System.Collections.Generic;
    using MyGameShop.WPFAPI.VM;

    /// <summary>
    /// Interface for the logic.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// Deletes a publisher.
        /// </summary>
        /// <param name="pub">pub to delete.</param>
        void ApiDelPub(PubVM pub);

        /// <summary>
        /// EDits a pub.
        /// </summary>
        /// <param name="pub">Pub to edit.</param>
        /// <param name="isEditing">Is edited.</param>
        /// <returns>if it was ok.</returns>
        bool ApiEditPub(PubVM pub, bool isEditing);

        /// <summary>
        /// Gets a publisher.
        /// </summary>
        /// <returns>List of pubs.</returns>
        List<PubVM> ApiGetPubs();

        /// <summary>
        /// Edit a publisher.
        /// </summary>
        /// <param name="pub">publisher.</param>
        /// <param name="editor">editor.</param>
        void EditPub(PubVM pub, Func<PubVM, bool> editor);

        /// <summary>
        /// Sends a msg.
        /// </summary>
        /// <param name="success">if it was ok.</param>
        void SendMessage(bool success);
    }
}