﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.WPFAPI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using MyGameShop.WPFAPI.VM;

    /// <summary>
    /// Logic for the app.
    /// </summary>
    public class MainLogic : IMainLogic
    {
        private string url = "https://localhost:44372/Api/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);
        private Factory factory;
        private IMessenger messengerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainLogic"/> class.
        /// </summary>
        /// <param name="messenger">messenger.</param>
        /// <param name="factory">factory class.</param>
        public MainLogic(IMessenger messenger, Factory factory)
        {
            this.messengerService = messenger;
            this.factory = factory;
        }

        /// <summary>
        /// Sends a msg.
        /// </summary>
        /// <param name="success">If it was ok.</param>
        public void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "PubResult");
        }

        /// <summary>
        /// Gets all the pubs.
        /// </summary>
        /// <returns>List of pubs.</returns>
        public List<PubVM> ApiGetPubs()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<PubVM>>(json, this.jsonOptions);
            return list;
        }

        /// <summary>
        /// Deletes a pub.
        /// </summary>
        /// <param name="pub">a pub.</param>
        public void ApiDelPub(PubVM pub)
        {
            bool success = false;
            if (pub != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + pub.PublisherId.ToString(CultureInfo.InvariantCulture)).Result;
                JsonDocument doc = JsonDocument.Parse(json);

                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(success);
        }

        /// <summary>
        /// Edits a publisher.
        /// </summary>
        /// <param name="pub">publisher to edit.</param>
        /// <param name="isEditing">if it is editing.</param>
        /// <returns>whether it was ok.</returns>
        public bool ApiEditPub(PubVM pub, bool isEditing)
        {
            if (pub == null)
            {
                return false;
            }

            string myUrl = isEditing ? this.url + "mod" : this.url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("publisherId", pub.PublisherId.ToString(CultureInfo.InvariantCulture));
            }

            postData.Add("country", pub.Country);
            postData.Add("name", pub.Name);
            postData.Add("ceo", pub.CEO);
            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).
                Result.Content.ReadAsStringAsync().Result;

            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <summary>
        /// Edits a publisher.
        /// </summary>
        /// <param name="pub">publisher to edit.</param>
        /// <param name="editor">editor.</param>
        public void EditPub(PubVM pub, Func<PubVM, bool> editor)
        {
            PubVM clone = new PubVM();
            if (pub != null)
            {
                clone.CopyFrom(pub);
            }

            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (pub != null)
                {
                    success = this.ApiEditPub(clone, true);
                }
                else
                {
                    success = this.ApiEditPub(clone, false);
                }
            }

            this.SendMessage(success == true);
        }
    }
}
