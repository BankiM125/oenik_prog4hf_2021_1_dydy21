﻿// <copyright file="PubVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.WPFAPI.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Publisher view model.
    /// </summary>
    public class PubVM : ObservableObject
    {
        private int publisherId;
        private string name;
        private string ceo;
        private string country;
        private bool doesSales;
        private bool isTripleA;

        /// <summary>
        /// Gets or sets id.
        /// </summary>
        public int PublisherId { get => this.publisherId; set => this.Set(ref this.publisherId, value); }

        /// <summary>
        /// Gets or sets name.
        /// </summary>
        public string Name { get => this.name; set => this.Set(ref this.name, value); }

        /// <summary>
        /// Gets or sets ceo.
        /// </summary>
        public string CEO { get => this.ceo; set => this.Set(ref this.ceo, value); }

        /// <summary>
        /// Gets or sets country.
        /// </summary>
        public string Country { get => this.country; set => this.Set(ref this.country, value); }

        /// <summary>
        /// Gets or sets a value indicating whether does sales.
        /// </summary>
        public bool DoesSales { get => this.doesSales; set => this.Set(ref this.doesSales, value); }

        /// <summary>
        /// Gets or sets a value indicating whether is triple a.
        /// </summary>
        public bool IsTripleA { get => this.isTripleA; set => this.Set(ref this.isTripleA, value); }

        /// <summary>
        /// Converts for view model pub.
        /// </summary>
        /// <param name="pub">publisher.</param>
        public void CopyFrom(PubVM pub)
        {
            if (pub is not null)
            {
                this.PublisherId = pub.PublisherId;
                this.Name = pub.Name;
                this.Country = pub.Country;
                this.CEO = pub.CEO;
                this.DoesSales = pub.DoesSales;
                this.IsTripleA = pub.isTripleA;
            }
        }
    }
}
