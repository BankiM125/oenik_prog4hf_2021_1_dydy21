﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.WPFAPI.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using MyGameShop.WPFAPI.BL;

    /// <summary>
    /// Main view model.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private IMainLogic logic;
        private Factory factory;
        private ObservableCollection<PubVM> allPubs;
        private PubVM selectedPub;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="logic">logic.</param>
        /// <param name="factory">factory.</param>
        public MainVM(IMainLogic logic, Factory factory)
        {
            this.logic = logic;
            this.factory = factory;
            this.LoadCmd = new RelayCommand(() => this.AllPubs = new ObservableCollection<PubVM>(this.logic.ApiGetPubs()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelPub(this.selectedPub));
            this.AddCmd = new RelayCommand(() => this.logic.EditPub(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditPub(this.selectedPub, this.EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
        : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainLogic>(), IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<Factory>())
        {
        }

        /// <summary>
        /// Gets or sets selected pub.
        /// </summary>
        public PubVM SelectedPub
        {
            get { return this.selectedPub; }

            set { this.Set(ref this.selectedPub, value); }
        }

        /// <summary>
        /// Gets or sets all pubs.
        /// </summary>
        public ObservableCollection<PubVM> AllPubs
        {
            get { return this.allPubs; }
            set { this.Set(ref this.allPubs, value); }
        }

        /// <summary>
        /// Gets or sets fonc for edits.
        /// </summary>
        public Func<PubVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets del command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets mod command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets load command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
