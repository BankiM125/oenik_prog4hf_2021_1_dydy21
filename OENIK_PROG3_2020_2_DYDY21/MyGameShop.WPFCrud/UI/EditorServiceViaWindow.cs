﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.WPFCrud.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyGameShop.Logic;
    using MyGameShop.WPFCrud.Data;

    /// <summary>
    /// Editor service via the window.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// To edit a publisher from the vm.
        /// </summary>
        /// <param name="p">What to edit.</param>
        /// <returns>If it did edit.</returns>
        public bool EditPub(WPFPublisher p)
        {
            EditorWindow win = new EditorWindow(p);
            return win.ShowDialog() ?? false;
        }
    }
}