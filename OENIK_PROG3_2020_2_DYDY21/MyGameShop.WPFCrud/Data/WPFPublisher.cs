﻿// <copyright file="WPFPublisher.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.WPFCrud.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Class for the view model publisher.
    /// </summary>
    public class WPFPublisher : ObservableObject
    {
        private int publisherId;
        private string name;
        private bool tripleA;
        private string country;
        private string cEO;
        private bool doesSales;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        public int PublisherId
        {
            get { return this.publisherId; }
            set { this.Set(ref this.publisherId, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether its triple a.
        /// </summary>
        public bool TripleA
        {
            get { return this.tripleA; }
            set { this.Set(ref this.tripleA, value); }
        }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public string Country
        {
            get { return this.country; }
            set { this.Set(ref this.country, value); }
        }

        /// <summary>
        /// Gets or sets the CEO name.
        /// </summary>
        public string CEO
        {
            get { return this.cEO; }
            set { this.Set(ref this.cEO, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether they do sales.
        /// </summary>
        public bool DoesSales
        {
            get { return this.doesSales; }
            set { this.Set(ref this.doesSales, value); }
        }

        /// <summary>
        /// Copies an entity.
        /// </summary>
        /// <param name="other">What to copy.</param>
        public void CopyFrom(WPFPublisher other)
        {
            this.GetType().GetProperties().ToList().
                ForEach(property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
