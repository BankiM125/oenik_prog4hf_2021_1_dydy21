﻿// <copyright file="IPublisherLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.WPFCrud.BL
{
    using System.Collections.Generic;
    using System.Linq;
    using MyGameShop.WPFCrud.Data;

    /// <summary>
    /// Publisher logic interface.
    /// </summary>
    public interface IPublisherLogic
    {
        /// <summary>
        /// Adds a publisher.
        /// </summary>
        /// <param name="list">List of pubs.</param>
        void AddPublisher(IList<WPFPublisher> list);

        /// <summary>
        /// Modifies a publisher.
        /// </summary>
        /// <param name="pubToMod">Which to mod.</param>
        void ModPublisher(WPFPublisher pubToMod);

        /// <summary>
        /// Deletes a pub from a list.
        /// </summary>
        /// <param name="list">From where to delete.</param>
        /// <param name="pub">What to delete.</param>
        void DelPublisher(IList<WPFPublisher> list, WPFPublisher pub);

        /// <summary>
        /// Gets all the entities.
        /// </summary>
        /// <returns>All the entities in the list.</returns>
        IList<WPFPublisher> GetAllPubs();
    }
}
