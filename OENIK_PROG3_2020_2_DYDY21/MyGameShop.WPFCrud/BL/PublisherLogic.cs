﻿// <copyright file="PublisherLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.WPFCrud.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using MyGameShop.Data;
    using MyGameShop.Logic;
    using MyGameShop.Repository;
    using MyGameShop.WPFCrud.Data;

    /// <summary>
    /// Publisher logic.
    /// </summary>
    public class PublisherLogic : IPublisherLogic
    {
        private IEditorService editorService;
        private IMessenger messengerService;
        private Factory factory;

        /// <summary>
        /// Initializes a new instance of the <see cref="PublisherLogic"/> class.
        /// </summary>
        /// <param name="service">Editor service.</param>
        /// <param name="messenger">Messenger service.</param>
        /// <param name="factory">Facitry class.</param>
        public PublisherLogic(IEditorService service, IMessenger messenger, Factory factory)
        {
            this.editorService = service;
            this.messengerService = messenger;
            this.factory = factory;
        }

        /// <inheritdoc/>
        public void AddPublisher(IList<WPFPublisher> list)
        {
            WPFPublisher newPub = new WPFPublisher();
            if (this.editorService.EditPub(newPub) == true && list is not null)
            {
                list.Add(newPub);
                Publisher publisher = new Publisher() { Name = newPub.Name, CEO = newPub.CEO, Country = newPub.Country, DoesSales = newPub.DoesSales, TripleA = newPub.TripleA, PublisherId = newPub.PublisherId };
                this.factory.PubRepo.Create(publisher);
                this.messengerService.Send("ADD OK", "LogicResult");
                this.factory.Ctx.SaveChanges();
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public void DelPublisher(IList<WPFPublisher> list, WPFPublisher pub)
        {
            if (pub != null && list is not null && list.Remove(pub))
            {
                this.factory.PubRepo.Delete(this.factory.PubRepo.GetOne(pub.PublisherId));
                this.messengerService.Send("DELETE OK", "LogicResult");
                this.factory.Ctx.SaveChanges();
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <inheritdoc/>
        public IList<WPFPublisher> GetAllPubs()
        {
            IList<Publisher> pubs = this.factory.PubRepo.GetAll().ToList();
            IList<WPFPublisher> publist = new List<WPFPublisher>();

            foreach (var item in pubs)
            {
                WPFPublisher newpublisher = new WPFPublisher() { Name = item.Name, CEO = item.CEO, Country = item.Country, DoesSales = item.DoesSales, TripleA = item.TripleA, PublisherId = item.PublisherId };
                publist.Add(newpublisher);
            }

            return publist;
        }

        /// <inheritdoc/>
        public void ModPublisher(WPFPublisher pubToMod)
        {
            if (pubToMod == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            WPFPublisher clone = new WPFPublisher();
            clone.CopyFrom(pubToMod);
            if (this.editorService.EditPub(clone) == true)
            {
                Publisher publisher = this.factory.PubRepo.GetOne(clone.PublisherId);
                publisher.Name = clone.Name;
                publisher.DoesSales = clone.DoesSales;
                publisher.Country = clone.Country;
                publisher.TripleA = clone.TripleA;
                publisher.CEO = clone.CEO;
                pubToMod.CopyFrom(clone);
                this.factory.PubLogic.Update(publisher, clone.CEO);
                this.messengerService.Send("EDIT OK", "LogicResult");
                this.factory.Ctx.SaveChanges();
            }
            else
            {
                this.messengerService.Send("EDIT CANCEL", "LogicResult");
            }
        }
    }
}
