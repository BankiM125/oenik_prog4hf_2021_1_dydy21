﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.WPFCrud
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyGameShop.WPFCrud.Data;

    /// <summary>
    /// Interface of Customer logic.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Interface to edit a publisher.
        /// </summary>
        /// <param name="p">Publisher to edit.</param>
        /// <returns>If it was successful.</returns>
        bool EditPub(WPFPublisher p);
    }
}
