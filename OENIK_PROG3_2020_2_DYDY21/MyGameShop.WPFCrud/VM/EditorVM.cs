﻿// <copyright file="EditorVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.WPFCrud.VM
{
    using GalaSoft.MvvmLight;
    using MyGameShop.WPFCrud.Data;

    /// <summary>
    /// Editor view model.
    /// </summary>
    public class EditorVM : ViewModelBase
    {
        private WPFPublisher pub;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorVM"/> class.
        /// </summary>
        public EditorVM()
        {
            this.pub = new WPFPublisher();
            if (this.IsInDesignMode)
            {
                this.pub.CEO = "Teszt Elek";
                this.pub.Country = "SK";
                this.pub.DoesSales = true;
                this.pub.TripleA = false;
                this.pub.Name = "OE Interactive";
            }
        }

        /// <summary>
        /// Gets or sets the Publisher class for the VM.
        /// </summary>
        public WPFPublisher Pub
        {
            get { return this.pub; }
            set { this.Set(ref this.pub, value); }
        }
    }
}
