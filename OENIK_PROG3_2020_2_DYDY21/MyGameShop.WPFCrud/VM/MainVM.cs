﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.WPFCrud.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using MyGameShop.Data;
    using MyGameShop.Logic;
    using MyGameShop.Repository;
    using MyGameShop.WPFCrud.BL;
    using MyGameShop.WPFCrud.Data;

    /// <summary>
    /// Main view manager.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private IPublisherLogic publisherlogic;
        private Factory factory;
        private WPFPublisher pubSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="publisherlogic">Logic of publishers.</param>
        /// <param name="factory">Factory class.</param>
        public MainVM(IPublisherLogic publisherlogic, Factory factory)
        {
            this.publisherlogic = publisherlogic;
            this.factory = factory;

            this.Team = new ObservableCollection<WPFPublisher>();

            if (this.IsInDesignMode)
            {
                WPFPublisher p2 = new WPFPublisher() { Name = "Wild Bill 2" };
                WPFPublisher p3 = new WPFPublisher() { Name = "Wild Bill 3", CEO = "Teszt Elek" };
                this.Team.Add(p2);
                this.Team.Add(p3);
            }
            else
            {
                if (publisherlogic is not null)
                {
                    foreach (var item in publisherlogic.GetAllPubs())
                    {
                        this.Team.Add(item);
                    }
                }
            }

            this.AddCmd = new RelayCommand(() => this.publisherlogic.AddPublisher(this.Team));
            this.ModCmd = new RelayCommand(() => this.publisherlogic.ModPublisher(this.PubSelected));
            this.DelCmd = new RelayCommand(() => this.publisherlogic.DelPublisher(this.Team, this.PubSelected));
            this.GetAll = new RelayCommand(() => this.publisherlogic.GetAllPubs());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IPublisherLogic>(), IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<Factory>())
        {
        }

        /// <summary>
        /// Gets or sets the Selected publisher.
        /// </summary>
        public WPFPublisher PubSelected
        {
            get { return this.pubSelected; }
            set { this.Set(ref this.pubSelected, value); }
        }

        /// <summary>
        /// Gets the collection of pubs.
        /// </summary>
        public ObservableCollection<WPFPublisher> Team { get; private set; }

        /// <summary>
        /// Gets the add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets the mod command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets the del command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets the get all command.
        /// </summary>
        public ICommand GetAll { get; private set; }
    }
}
