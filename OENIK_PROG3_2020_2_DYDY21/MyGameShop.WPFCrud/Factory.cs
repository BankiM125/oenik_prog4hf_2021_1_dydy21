﻿// <copyright file="Factory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.WPFCrud
{
    using MyGameShop.Data;
    using MyGameShop.Logic;
    using MyGameShop.Repository;

    /// <summary>
    /// Factory class.
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Factory"/> class.
        /// </summary>
        public Factory()
        {
            this.Ctx = new GameShopDbContext();
            this.PubRepo = new PublisherRepository(this.Ctx);
            this.GameRepo = new GameRepository(this.Ctx);
            this.PlatRepo = new PlatformRepository(this.Ctx);
            this.PubLogic = new AdminLogic(this.GameRepo, this.PlatRepo, this.PubRepo);
        }

        /// <summary>
        /// Gets or sets the Context for the db.
        /// </summary>
        public GameShopDbContext Ctx { get; set; }

        /// <summary>
        /// Gets or sets the publisher repo.
        /// </summary>
        public IPublisherRepository PubRepo { get; set; }

        /// <summary>
        /// Gets or sets the game repo.
        /// </summary>
        public IGameRepository GameRepo { get; set; }

        /// <summary>
        /// Gets or sets the platform repo.
        /// </summary>
        public IPlatformRepository PlatRepo { get; set; }

        /// <summary>
        /// Gets or sets the publisher logic.
        /// </summary>
        public IAdminLogic<Publisher> PubLogic { get; set; }
    }
}
