﻿// <copyright file="PubsController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using MyGameShop.Web.Models;

    /// <summary>
    /// Controller of publishers.
    /// </summary>
    public class PubsController : Controller
    {
        private Factory factory;
        private IMapper mapper;
        private PubViewModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="PubsController"/> class.
        /// </summary>
        /// <param name="factory">Factory class.</param>
        /// <param name="mapper">Mapper.</param>
        public PubsController(Factory factory, IMapper mapper)
        {
            this.factory = factory;
            this.mapper = mapper;

            model = new PubViewModel();
            model.EditedPub = new Models.Publisher();

            var pubs = this.factory.PubCustLogic.GetAll();
            model.ListOfPubs = this.mapper.Map<IList<Data.Publisher>, List<Models.Publisher>>(pubs);
        }

        /// <summary>
        /// Index action result.
        /// </summary>
        /// <returns>Publisher List view.</returns>
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return this.View("PubsIndex", model);
        }

        /// <summary>
        /// Details action.
        /// </summary>
        /// <param name="id">Id of publisher.</param>
        /// <returns>Pubs detail view.</returns>
        public ActionResult Details(int id)
        {
            return this.View("PubsDetails", this.GetPubModel(id));
        }

        /// <summary>
        /// Remove action.
        /// </summary>
        /// <param name="id">Id of publisher to remove.</param>
        /// <returns>Index view.</returns>
        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";
            if (factory.PubCustLogic.GetOne(id) is not null)
            {
                factory.PubLogic.Remove(factory.PubCustLogic.GetOne(id));
                TempData["editResult"] = "Delete OK";
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Edit action.
        /// </summary>
        /// <param name="id">Id of pub to edit.</param>
        /// <returns>Pubs Edit view.</returns>
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            model.EditedPub = GetPubModel(id);
            return View("PubsIndex", model);
        }

        /// <summary>
        /// Edit action.
        /// </summary>
        /// <param name="pub">Publisher to edit.</param>
        /// <param name="editAction">edit action.</param>
        /// <returns>Redirects to index.</returns>
        [HttpPost]
        public ActionResult Edit(Models.Publisher pub, string editAction)
        {
            if (ModelState.IsValid && pub != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    try
                    {
                        factory.PubLogic.Create(new MyGameShop.Data.Publisher() { Name = pub.Name, CEO = pub.CEO, Country = pub.Country, DoesSales = pub.DoesSales, TripleA = pub.TripleA, PublisherId = pub.PublisherId });
                    }
                    catch (ArgumentException ex)
                    {
                        TempData["editResult"] = "Insert FAIL: " + ex.Message;
                    }
                }
                else
                {
                    if (factory.PubRepo.GetOne(pub.PublisherId) is null)
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                    else
                    {
                        factory.PubRepo.Update(factory.PubRepo.GetOne(pub.PublisherId), pub.CEO);
                    }
                }

                return RedirectToAction("Index");
            }
            else
            {
                ViewData["editAction"] = "Edit";
                model.EditedPub = pub;
                return View("PubsIndex", model);
            }
        }

        private Models.Publisher GetPubModel(int id)
        {
            Data.Publisher dataPub = this.factory.PubCustLogic.GetOne(id);
            return mapper.Map<Data.Publisher, Models.Publisher>(dataPub);
        }
    }
}
