﻿// <copyright file="ApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Microsoft.AspNetCore.Mvc;
    using MyGameShop.Data;
    using MyGameShop.Logic;
    using MyGameShop.Web.Models;

    /// <summary>
    /// Api controller class.
    /// </summary>
    public class ApiController : Controller
    {
        private Factory factory;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiController"/> class.
        /// </summary>
        /// <param name="factory">factory.</param>
        /// <param name="mapper">mapper.</param>
        public ApiController(Factory factory, IMapper mapper)
        {
            this.factory = factory;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets all the pubs.
        /// </summary>
        /// <returns>Enumerable of pubs.</returns>
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Publisher> GetAll()
        {
            var pubs = factory.PubCustLogic.GetAll();
            return mapper.Map<IList<Data.Publisher>, List<Models.Publisher>>(pubs);
        }

        /// <summary>
        /// Deletes a pub.
        /// </summary>
        /// <param name="id">id of pub.</param>
        /// <returns>Api result.</returns>
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOnePub(int id)
        {
            if (factory.PubCustLogic.GetOne(id) is not null)
            {
                factory.PubLogic.Remove(factory.PubCustLogic.GetOne(id));
                return new ApiResult() { OperationResult = true };
            }
            else
            {
                return new ApiResult() { OperationResult = false };
            }
        }

        /// <summary>
        /// Adds a pub.
        /// </summary>
        /// <param name="pub">publisher.</param>
        /// <returns>api result.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOnePub(Models.Publisher pub)
        {
            bool success = true;
            if (pub is not null)
            {
                factory.PubLogic.Create(new MyGameShop.Data.Publisher() { Name = pub.Name, CEO = pub.CEO, Country = pub.Country, DoesSales = pub.DoesSales, TripleA = pub.TripleA, PublisherId = pub.PublisherId });
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Modifies a pub.
        /// </summary>
        /// <param name="pub">Model publisher.</param>
        /// <returns>api result.
        /// </returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOnePub(Models.Publisher pub)
        {
            if (pub is not null)
            {
                Data.Publisher asd = factory.PubRepo.GetOne(pub.PublisherId);
                factory.PubRepo.Update(asd, pub.CEO);
                return new ApiResult() { OperationResult = true };
            }
            else
            {
                return new ApiResult() { OperationResult = false };
            }
        }
    }
}
