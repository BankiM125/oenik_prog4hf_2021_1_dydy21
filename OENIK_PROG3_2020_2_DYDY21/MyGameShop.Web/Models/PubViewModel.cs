﻿// <copyright file="PubViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using MyGameShop.Data;

    /// <summary>
    /// Pub view model.
    /// </summary>
    public class PubViewModel
    {
        /// <summary>
        /// Gets or sets edited publisher.
        /// </summary>
        public Publisher EditedPub { get; set; }

        /// <summary>
        /// Gets or sets list of edited pubs.
        /// </summary>
        public List<Publisher> ListOfPubs { get; set; }
    }
}
