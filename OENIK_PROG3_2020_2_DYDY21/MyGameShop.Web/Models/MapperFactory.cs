﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// Mapper factory.
    /// </summary>
    public class MapperFactory
    {
        /// <summary>
        /// Creates a mapper.
        /// </summary>
        /// <returns>the mapper.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MyGameShop.Data.Publisher, MyGameShop.Web.Models.Publisher>().
                    ForMember(dest => dest.PublisherId, map => map.MapFrom(src => src.PublisherId)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                    ForMember(dest => dest.CEO, map => map.MapFrom(src => src.CEO)).
                    ForMember(dest => dest.DoesSales, map => map.MapFrom(src => src.DoesSales)).
                    ForMember(dest => dest.TripleA, map => map.MapFrom(src => src.TripleA)).
                    ForMember(dest => dest.Country, map => map.MapFrom(src => src.Country));
            });
            return config.CreateMapper();
        }
    }
}
