﻿// <copyright file="Publisher.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Publisher class.
    /// </summary>
    public class Publisher
    {
        /// <summary>
        /// Gets or sets iD of pub.
        /// </summary>
        [Display(Name = "Publisher id")]
        public int PublisherId { get; set; }

        /// <summary>
        /// Gets or sets name of pub.
        /// </summary>
        [Display(Name = "Publisher name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether if pub is tripple a.
        /// </summary>
        [Display(Name = "Does Triple A")]
        public bool TripleA { get; set; }

        /// <summary>
        /// Gets or sets country of pub.
        /// </summary>
        [Display(Name = "Publisher Country")]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets cEO of pub.
        /// </summary>
        [Display(Name = "Publisher CEO")]
        public string CEO { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the pub does sales.
        /// </summary>
        [Display(Name = "Does Sales")]
        public bool DoesSales { get; set; }
    }
}
