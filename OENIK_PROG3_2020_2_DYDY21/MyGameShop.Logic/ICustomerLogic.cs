﻿// <copyright file="ICustomerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Logic
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface of Customer logic.
    /// </summary>
    /// <typeparam name="T"> Generic paramter.</typeparam>
    public interface ICustomerLogic<T>
        where T : class
    {
        /// <summary>
        /// Gets an entity.
        /// </summary>
        /// <param name="id"> Id to get.</param>
        /// <returns> Entity.</returns>
        T GetOne(int id);

        /// <summary>
        /// Gets all the entities.
        /// </summary>
        /// <returns>A list of entities.</returns>
        IList<T> GetAll();
    }
}
