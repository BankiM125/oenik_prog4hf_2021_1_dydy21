﻿// <copyright file="Average.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Logic
{
    /// <summary>
    /// Class for the avg price.
    /// </summary>
    public class Average
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the avg price value of the game.
        /// </summary>
        public double Avg { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.Name}: {this.Avg}";
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (!(obj is Average item))
            {
                return false;
            }

            return this.Name == item.Name && this.Avg == item.Avg;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
