﻿// <copyright file="IAdminLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Logic
{
    using MyGameShop.Data;

    /// <summary>
    /// Interface for admin logic.
    /// </summary>
    /// <typeparam name="T"> Generic parameter.</typeparam>
    public interface IAdminLogic<T> : ICustomerLogic<Game>, ICustomerLogic<Platform>, ICustomerLogic<Publisher>
        where T : class
    {
        /// <summary>
        /// Creates an entity.
        /// </summary>
        /// <param name="entity">Entity to create.</param>
        void Create(T entity);

        /// <summary>
        /// Removes an entity.
        /// </summary>
        /// <param name="entity"> Entity to remove.</param>
        void Remove(T entity);

        /// <summary>
        /// Update an entity.
        /// </summary>
        /// <param name="entity">Entity to update.</param>
        /// <param name="value">New value.</param>
        void Update(T entity, string value);
    }
}
