﻿// <copyright file="GamesOfPubs.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Logic
{
    using System.Collections.Generic;
    using MyGameShop.Data;

    /// <summary>
    /// Class for games of publishers for the query.
    /// </summary>
    public class GamesOfPubs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GamesOfPubs"/> class.
        /// </summary>
        /// <param name="games">List of Games.</param>
        public GamesOfPubs(ICollection<Game> games)
        {
            this.Games = games;
        }

        /// <summary>
        /// Gets the name of the game.
        /// </summary>
        public ICollection<Game> Games { get; }

        /// <summary>
        /// Gets or sets the name of the publisher.
        /// </summary>
        public string PublisherName { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            string kimenet = this.PublisherName + " :";
            foreach (var item in this.Games)
            {
                kimenet += " " + item.GameName + ", ";
            }

            return kimenet;
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (!(obj is GamesOfPubs item))
            {
                return false;
            }

            return this.PublisherName == item.PublisherName && this.Games.Equals(item.Games);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
