﻿// <copyright file="AdminLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using GalaSoft.MvvmLight.Messaging;
    using MyGameShop.Data;
    using MyGameShop.Repository;

    /// <summary>
    /// Class for the admin logic.
    /// </summary>
    public class AdminLogic : CustomerLogic, IAdminLogic<Game>, IAdminLogic<Platform>, IAdminLogic<Publisher>
    {
        /// <summary>
        /// Source of game data.
        /// </summary>
        private readonly IGameRepository gameDataSource;
        private readonly IPlatformRepository platformDataSource;
        private readonly IPublisherRepository publisherDataSource;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdminLogic"/> class.
        /// </summary>
        /// <param name="games"> Interface of the Game repository.</param>
        /// <param name="platforms"> Interface of the Platform repository.</param>
        /// <param name="publishers"> Interface of the Publisher repository.</param>
        public AdminLogic(IGameRepository games, IPlatformRepository platforms, IPublisherRepository publishers)
            : base(games, platforms, publishers)
        {
            this.gameDataSource = games;
            this.platformDataSource = platforms;
            this.publisherDataSource = publishers;
        }

        /// <summary>
        /// Creates a game.
        /// </summary>
        /// <param name="entity"> Game to create.</param>
        public void Create(Game entity)
        {
            this.gameDataSource.Create(entity);
        }

        /// <summary>
        /// Creates a platform.
        /// </summary>
        /// <param name="entity"> Platform to create.</param>
        public void Create(Publisher entity)
        {
            this.publisherDataSource.Create(entity);
        }

        /// <summary>
        /// creates a publisher.
        /// </summary>
        /// <param name="entity"> Publisher to create.</param>
        public void Create(Platform entity)
        {
            this.platformDataSource.Create(entity);
        }

        /// <summary>
        /// Removes a game.
        /// </summary>
        /// <param name="entity"> Game to remove.</param>
        public void Remove(Game entity)
        {
            this.gameDataSource.Delete(entity);
        }

        /// <summary>
        /// Removes a platform.
        /// </summary>
        /// <param name="entity"> Platform to remove.</param>
        public void Remove(Platform entity)
        {
            this.platformDataSource.Delete(entity);
        }

        /// <summary>
        /// Removes a publisher.
        /// </summary>
        /// <param name="entity"> Publisher to remove.</param>
        public void Remove(Publisher entity)
        {
            this.publisherDataSource.Delete(entity);
        }

        /// <summary>
        /// Updates the price of a game.
        /// </summary>
        /// <param name="entity">Game object.</param>
        /// <param name="value">Price of the game.</param>
        public void Update(Game entity, string value)
        {
            if (int.TryParse(value, out int id))
            {
                this.gameDataSource.Update(entity, id);
            }
        }

        /// <summary>
        /// Changes the version of a platform.
        /// </summary>
        /// <param name="entity">Platform to update.</param>
        /// <param name="value">Vesrion to update to.</param>
        public void Update(Platform entity, string value)
        {
            if (int.TryParse(value, out int id))
            {
                this.platformDataSource.Update(entity, id);
            }
        }

        /// <summary>
        /// Changes the CEO of a publisher.
        /// </summary>
        /// <param name="entity">Publisher to update.</param>
        /// <param name="value">Name of CEO.</param>
        public void Update(Publisher entity, string value)
        {
            if (int.TryParse(value, out int id))
            {
                this.publisherDataSource.Update(entity, value);
            }
        }
    }
}
