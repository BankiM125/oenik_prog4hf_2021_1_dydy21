﻿// <copyright file="CustomerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyGameShop.Data;
    using MyGameShop.Repository;

    /// <summary>
    /// Logic for games.
    /// </summary>
    public class CustomerLogic : ICustomerLogic<Game>, ICustomerLogic<Publisher>, ICustomerLogic<Platform>
    {
        /// <summary>
        /// Source of game data.
        /// </summary>
        private readonly IGameRepository gameDataSource;
        private readonly IPlatformRepository platformDataSource;
        private readonly IPublisherRepository publisherDataSource;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerLogic"/> class.
        /// </summary>
        /// <param name="games"> Repo of games.</param>
        /// <param name="platforms"> Repo of platforms.</param>
        /// <param name="publishers"> Repo of publishers.</param>
        public CustomerLogic(IGameRepository games, IPlatformRepository platforms, IPublisherRepository publishers)
        {
            this.gameDataSource = games;
            this.platformDataSource = platforms;
            this.publisherDataSource = publishers;
        }

        /// <summary>
        /// Query of the genres from publishers.
        /// </summary>
        /// <returns> A list of genres and publishers.</returns>
        public IList<GamesOfPubs> GetGenresOfPublishers()
        {
            var q = from games in this.gameDataSource.GetAll()
                    join publishers in this.publisherDataSource.GetAll() on games.PublisherId
                    equals publishers.PublisherId
                    select new GamesOfPubs(publishers.Games)
                    {
                        PublisherName = publishers.Name,
                    };

            return q.OrderBy(x => x.PublisherName).ToList();
        }

        /// <summary>
        /// Query of the genres.
        /// </summary>
        /// <returns> A list of genres and publishers.</returns>
        public IList<string> GetGameGenres()
        {
            var q = from games in this.gameDataSource.GetAll()
                    select games.Genre;

            return q.ToList();
        }

        /// <summary>
        /// Query of games on platforms.
        /// </summary>
        /// <returns> A list of games on different platforms.</returns>
        public IList<PlatformGame> GamesOnPlatform()
        {
            var q = from platforms in this.platformDataSource.GetAll()
                    join games in this.gameDataSource.GetAll() on platforms.PlatformId
                    equals games.PlatformId
                    select new PlatformGame(platforms.Games) { Platform = platforms.Name, };

            return q.OrderBy(x => x.Platform).ToList();
        }

        /// <summary>
        /// Query of avg prices on a platform.
        /// </summary>
        /// <returns> A list of avg prices on platforms.</returns>
        public IList<Average> AverageOnPlatform()
        {
        var q = from games in this.gameDataSource.GetAll()
                join platforms in this.platformDataSource.GetAll() on games.PlatformId
                equals platforms.PlatformId
                select new { platforms.Name, games.Price, }
                into g
                group g by new { g.Name } into f
                select new Average
                {
                     Name = f.Key.Name,
                     Avg = f.Average(b => b.Price) ?? 0,
                };

        return q.OrderBy(x => x.Avg).ToList();
        }

        /// <summary>
        /// Async method to get the Averages.
        /// </summary>
        /// <returns>Task to run the AverageOnPlatform method.</returns>
        public Task<IList<Average>> AverageOnPlatformAsync()
        {
            return Task.Run(() => this.AverageOnPlatform());
        }

        /// <summary>
        /// Async method for to list the games on a platform.
        /// </summary>
        /// <returns>Task to run the GamesOnPlatform method.</returns>
        public Task<IList<PlatformGame>> GamesOnPlatformAsync()
        {
            return Task.Run(() => this.GamesOnPlatform());
        }

        /// <summary>
        /// Async method to get the genres on different platforms.
        /// </summary>
        /// <returns>Task to run the GetGenresOfPublishers method.</returns>
        public Task<IList<GamesOfPubs>> GetGenresOfPublishersAsync()
        {
            return Task.Run(() => this.GetGenresOfPublishers());
        }

        /// <inheritdoc/>
        public IList<Game> GetAll()
        {
            return this.gameDataSource.GetAll().ToList();
        }

        /// <inheritdoc/>
        public Game GetOne(int id)
        {
            return this.gameDataSource.GetOne(id);
        }

        /// <inheritdoc/>
        IList<Publisher> ICustomerLogic<Publisher>.GetAll()
        {
            return this.publisherDataSource.GetAll().ToList();
        }

        /// <inheritdoc/>
        IList<Platform> ICustomerLogic<Platform>.GetAll()
        {
            return this.platformDataSource.GetAll().ToList();
        }

        /// <inheritdoc/>
        Publisher ICustomerLogic<Publisher>.GetOne(int id)
        {
            return this.publisherDataSource.GetOne(id);
        }

        /// <inheritdoc/>
        Platform ICustomerLogic<Platform>.GetOne(int id)
        {
            return this.platformDataSource.GetOne(id);
        }
    }
}
