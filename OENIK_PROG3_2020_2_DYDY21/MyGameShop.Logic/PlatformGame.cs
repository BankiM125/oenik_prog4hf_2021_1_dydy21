﻿// <copyright file="PlatformGame.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Logic
{
    using System.Collections.Generic;
    using MyGameShop.Data;

    /// <summary>
    /// PlatformGame class for the customer logic query.
    /// </summary>
    public class PlatformGame
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlatformGame"/> class.
        /// </summary>
        /// <param name="games">List of Games.</param>
        public PlatformGame(ICollection<Game> games)
        {
            this.Games = games;
        }

        /// <summary>
        /// Gets the name of the game.
        /// </summary>
        public ICollection<Game> Games { get; }

        /// <summary>
        /// Gets or sets the name of the platform.
        /// </summary>
        public string Platform { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            string kimenet = this.Platform + " :";
            foreach (var item in this.Games)
            {
                kimenet += " " + item.GameName + ", ";
            }

            return kimenet;
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (!(obj is PlatformGame item))
            {
                return false;
            }

            return this.Platform == item.Platform && this.Games.Equals(item.Games);
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
