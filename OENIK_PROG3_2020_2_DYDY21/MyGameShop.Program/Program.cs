﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
/*
namespace MyGameShop.Program
{
    using System;
    using System.Linq;
    using ConsoleTools;
    using MyGameShop.Data;
    using MyGameShop.Logic;
    using MyGameShop.Repository;
*/

    /// <summary>
    /// Main program.
    /// </summary>
    internal class Program
    {
        private static void Main()
        {
        /*
            GameShopDbContext gamesDbContext = new GameShopDbContext();
            IGameRepository games = new GameRepository(gamesDbContext);
            IPlatformRepository platforms = new PlatformRepository(gamesDbContext);
            IPublisherRepository publishers = new PublisherRepository(gamesDbContext);
            ICustomerLogic<Game> customerGameLogic = new CustomerLogic(games, platforms, publishers);
            ICustomerLogic<Publisher> customerPublisherLogic = new CustomerLogic(games, platforms, publishers);
            ICustomerLogic<Platform> customerPlatformLogic = new CustomerLogic(games, platforms, publishers);
            CustomerLogic generalCustoemrLogic = new CustomerLogic(games, platforms, publishers);
            IAdminLogic<Game> adminGameLogic = new AdminLogic(games, platforms, publishers);
            IAdminLogic<Publisher> adminPublisherLogic = new AdminLogic(games, platforms, publishers);
            IAdminLogic<Platform> adminPlatformLogic = new AdminLogic(games, platforms, publishers);
            var provider = System.Globalization.CultureInfo.InvariantCulture;

            var menu = new ConsoleMenu()
              .Add("Add new game", () =>
              {
                  Console.WriteLine("Set name");
                  string name = Console.ReadLine();
                  Console.WriteLine("Set genre");
                  string genre = Console.ReadLine();
                  Console.WriteLine("Set price");
                  bool priceSuccessful = int.TryParse(Console.ReadLine(), out int price);
                  Console.WriteLine("Set platformid");
                  bool platformidSuccessful = int.TryParse(Console.ReadLine(), out int platformid);
                  Console.WriteLine("Set publisherid");
                  bool publisheridSuccessful = int.TryParse(Console.ReadLine(), out int publisherid);
                  Console.WriteLine("Set release date ");
                  bool releaseSuccessful = DateTime.TryParse(Console.ReadLine(), out DateTime release);
                  if (platformidSuccessful && publisheridSuccessful && releaseSuccessful && priceSuccessful)
                  {
                      Game newgame = new Game() { GameName = name, Price = price, PublisherId = publisherid, Genre = genre, Release = release, PlatformId = platformid };
                      adminGameLogic.Create(newgame);
                      Console.WriteLine("added new game");
                  }
                  else
                  {
                      Console.WriteLine("Please enter valid parameters for the new game.");
                  }

                  Console.ReadKey();
              })
              .Add("Add new platform", () =>
              {
                  Console.WriteLine("Set name");
                  string name = Console.ReadLine();
                  Console.WriteLine("Set ceo");
                  string ceo = Console.ReadLine();
                  Console.WriteLine("Set country");
                  string country = Console.ReadLine();
                  Console.WriteLine("Set whether they do sales true/false");
                  bool doessalesSuccessful = bool.TryParse(Console.ReadLine(), out bool doessales);
                  Console.WriteLine("Set whether they are triple A true/false");
                  bool tripleaSuccessful = bool.TryParse(Console.ReadLine(), out bool triplea);
                  if (doessalesSuccessful && tripleaSuccessful)
                  {
                      Publisher newpub = new Publisher() { Name = name, CEO = ceo, Country = country, DoesSales = doessales, TripleA = triplea };
                      adminPublisherLogic.Create(newpub);
                      Console.WriteLine("added new publisher");
                  }
                  else
                  {
                      Console.WriteLine("Please enter valid paramters for the new platform.");
                  }

                  Console.ReadKey();
              })
              .Add("Add new publisher", () =>
              {
                  Console.WriteLine("Set name");
                  string name = Console.ReadLine();
                  Console.WriteLine("Set shortname");
                  string shortname = Console.ReadLine();
                  Console.WriteLine("Set coorporation name");
                  string coorporation = Console.ReadLine();
                  Console.WriteLine("Set whether they have paid online services true/false");
                  bool paidonlineSuccessful = bool.TryParse(Console.ReadLine(), out bool paidonline);
                  Console.WriteLine("Set the version number");
                  bool versionSuccessful = int.TryParse(Console.ReadLine(), out int version);
                  Console.WriteLine("Set the number of users");
                  bool numberofusersSuccessful = int.TryParse(Console.ReadLine(), out int numberofusers);
                  if (paidonlineSuccessful && versionSuccessful && numberofusersSuccessful)
                  {
                      Platform newplat = new Platform() { Name = name, ShortName = shortname, Corporation = coorporation, PaidOnlineService = paidonline, Version = version, NumberOfUsers = numberofusers };
                      adminPlatformLogic.Create(newplat);
                      Console.WriteLine("added new platform");
                  }
                  else
                  {
                      Console.WriteLine("Please enter valid paramters for the new publisher");
                  }

                  Console.ReadKey();
              })
              .Add("Remove a game", () =>
              {
                  Console.WriteLine("ID of the game you would like to remove:");
                  bool idSuccessful = int.TryParse(Console.ReadLine(), out int id);
                  if (idSuccessful && !(gamesDbContext.Games.Find(id) is null))
                  {
                      adminGameLogic.Remove(gamesDbContext.Games.Find(id));
                      Console.WriteLine("Item was removed.");
                  }
                  else
                  {
                      Console.WriteLine("Please enter the ID of an existing item.");
                  }

                  Console.ReadKey();
              })
              .Add("Remove a platform", () =>
              {
                  Console.WriteLine("ID of the platform you would like to remove:");
                  bool idSuccessful = int.TryParse(Console.ReadLine(), out int id);
                  if (idSuccessful && !(gamesDbContext.Platforms.Find(id) is null))
                  {
                      adminPlatformLogic.Remove(gamesDbContext.Platforms.Find(id));
                      Console.WriteLine("Item was removed.");
                  }
                  else
                  {
                      Console.WriteLine("Please enter the ID of an existing item.");
                  }

                  Console.ReadKey();
              })
              .Add("Remove a publisher", () =>
              {
                  Console.WriteLine("ID of the publisher you would like to remove:");
                  bool idSuccessful = int.TryParse(Console.ReadLine(), out int id);
                  if (idSuccessful && !(gamesDbContext.Publishers.Find(id) is null))
                  {
                      adminPublisherLogic.Remove(gamesDbContext.Publishers.Find(id));
                      Console.WriteLine("Item was removed.");
                  }
                  else
                  {
                      Console.WriteLine("Please enter the ID of an existing item.");
                  }

                  Console.ReadKey();
              })
              .Add("List all the games", () =>
              {
                  Console.WriteLine("Number of games: " + gamesDbContext.Games.Count());
                  var allGames = games.GetAll();
                  foreach (var game in allGames)
                  {
                      Console.WriteLine(game.GameName);
                  }

                  Console.ReadKey();
              })
              .Add("List all the platforms", () =>
              {
                  Console.WriteLine("Number of platforms: " + gamesDbContext.Platforms.Count());
                  var allPlat = platforms.GetAll();
                  foreach (var plat in allPlat)
                  {
                      Console.WriteLine(plat.Name);
                  }

                  Console.ReadKey();
              })
              .Add("List all the publishers", () =>
              {
                  Console.WriteLine("Number of publishers: " + gamesDbContext.Publishers.Count());
                  var allPub = publishers.GetAll();
                  foreach (var pub in allPub)
                  {
                      Console.WriteLine(pub.Name);
                  }

                  Console.ReadKey();
              })
              .Add("Change the price of a game", () =>
              {
                  Console.WriteLine("ID of the game you would like the change the price of:");
                  bool idSuccessful = int.TryParse(Console.ReadLine(), out int id);
                  Console.WriteLine("Set a new price");
                  string price = Console.ReadLine();
                  if (idSuccessful && !(gamesDbContext.Games.Find(id) is null))
                  {
                      adminGameLogic.Update(games.GetOne(id), price);
                      Console.WriteLine("Update successful.");
                  }
                  else
                  {
                      Console.WriteLine("Please enter a valid item and value to update.");
                  }

                  Console.ReadKey();
              })
              .Add("Change the ceo of a publisher", () =>
              {
                  Console.WriteLine("ID of the publisher you would like the change the CEO of:");
                  bool idSuccessful = int.TryParse(Console.ReadLine(), out int id);
                  Console.WriteLine("Set a new CEO name");
                  string ceo = Console.ReadLine();
                  if (idSuccessful && !(gamesDbContext.Publishers.Find(id) is null))
                  {
                      adminPublisherLogic.Update(publishers.GetOne(id), ceo);
                      Console.WriteLine("Update successful.");
                  }
                  else
                  {
                      Console.WriteLine("Please enter a valid item and value to update.");
                  }

                  Console.ReadKey();
              })
              .Add("Change the version of a platform", () =>
              {
                  Console.WriteLine("ID of the platform you would like the change the version of:");
                  bool idSuccessful = int.TryParse(Console.ReadLine(), out int id);
                  Console.WriteLine("Set a new version");
                  string version = Console.ReadLine();
                  if (idSuccessful && !(gamesDbContext.Platforms.Find(id) is null))
                  {
                      adminPlatformLogic.Update(platforms.GetOne(id), version);
                      Console.WriteLine("Update successful.");
                  }
                  else
                  {
                      Console.WriteLine("Please enter a valid item and value to update.");
                  }

                  Console.ReadKey();
              })
              .Add("Get the average price of games on different platforms", () =>
              {
                  foreach (var item in generalCustoemrLogic.AverageOnPlatform())
                  {
                      Console.WriteLine(item);
                  }

                  Console.ReadKey();
              })
              .Add("Get the list of games from different publishers", () =>
              {
                  var distinctItems = generalCustoemrLogic.GetGenresOfPublishers().GroupBy(x => x.PublisherName).Select(y => y.First());
                  foreach (var item in distinctItems)
                  {
                      Console.WriteLine(item);
                  }

                  Console.ReadKey();
              })
              .Add("Get the what games each platform has", () =>
              {
                  var distinctItems = generalCustoemrLogic.GamesOnPlatform().GroupBy(x => x.Platform).Select(y => y.First());
                  foreach (var item in distinctItems)
                  {
                      Console.WriteLine(item);
                  }

                  Console.ReadKey();
              })
              .Add("Async: Get the average price of games on different platforms", () =>
              {
                  var res = generalCustoemrLogic.AverageOnPlatformAsync().Result;
                  foreach (var item in res)
                  {
                      Console.WriteLine(item);
                  }

                  Console.ReadKey();
              })
              .Add("Async: Get the list of games from different publishers", () =>
              {
                  var res = generalCustoemrLogic.GetGenresOfPublishersAsync().Result.GroupBy(x => x.PublisherName).Select(y => y.First());
                  foreach (var item in res)
                  {
                      Console.WriteLine(item);
                  }

                  Console.ReadKey();
              })
              .Add("Async: Get the what games each platform has", () =>
              {
                  var res = generalCustoemrLogic.GamesOnPlatformAsync().Result.GroupBy(x => x.Platform).Select(y => y.First());
                  foreach (var item in res)
                  {
                      Console.WriteLine(item);
                  }

                  Console.ReadKey();
              })
              .Add("Close", ConsoleMenu.Close);

            menu.Show();
            gamesDbContext.Dispose();
            Console.ReadKey();
        }*/
    }
}
