﻿// <copyright file="GameShopDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Data
{
    using System;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Database context for the program.
    /// </summary>
    public partial class GameShopDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameShopDbContext"/> class.
        /// </summary>
        public GameShopDbContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameShopDbContext"/> class.
        /// </summary>
        /// <param name="options"> options if specified.</param>
        public GameShopDbContext(DbContextOptions<GameShopDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Gets or sets the Database for games.
        /// </summary>
        public virtual DbSet<Game> Games { get; set; }

        /// <summary>
        /// Gets or sets the Database for publishers.
        /// </summary>
        public virtual DbSet<Publisher> Publishers { get; set; }

        /// <summary>
        /// Gets or sets the Database for platforms.
        /// </summary>
        public virtual DbSet<Platform> Platforms { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies()
                   .UseSqlServer(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = |DataDirectory|\GameDb.mdf; Integrated Security = True");
            }
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Publisher cDPrjectRed = new Publisher() { PublisherId = 1, Name = "CDProjectRed", CEO = "Will Smith", Country = "Poland", DoesSales = true, TripleA = true };
            Publisher riot = new Publisher() { PublisherId = 2, Name = "Riot Games", CEO = "John Brown", Country = "USA", DoesSales = false, TripleA = false };
            Publisher bethesda = new Publisher() { PublisherId = 3, Name = "Bethesda Softworks", CEO = "Tom Notch", Country = "USA", DoesSales = false, TripleA = true };
            Publisher valve = new Publisher() { PublisherId = 4, Name = "Valve Corporation", CEO = "Gabe Newell", Country = "USA", DoesSales = false, TripleA = true };
            Publisher eA = new Publisher() { PublisherId = 5, Name = "EA Games", CEO = "John Smith", Country = "Canada", DoesSales = true, TripleA = true };

            Game cyberpunk = new Game() { Id = 1, GameName = "Cyberpunk 2077", Price = 60, PublisherId = 1, Genre = "Action", Release = new DateTime(2020, 12, 05), PlatformId = 1 };
            Game leagueOfLegends = new Game() { Id = 2, GameName = "VALORANT", Price = 12, PublisherId = 2, Genre = "Shooter", Release = new DateTime(2020, 04, 10), PlatformId = 2 };
            Game witcher3 = new Game() { Id = 3, GameName = "The Witcher 3", Price = 25, PublisherId = 1, Genre = "RPG", Release = new DateTime(2015, 05, 18), PlatformId = 3 };
            Game cSGO = new Game() { Id = 4, GameName = "Counter Strike: Global Offensive", Price = 20, PublisherId = 4, Genre = "Shooter", Release = new DateTime(2012, 07, 21), PlatformId = 1 };
            Game skyrim = new Game() { Id = 5, GameName = "The Elder Scrolls V: Skyrim", Price = 20, PublisherId = 3, Genre = "RPG", Release = new DateTime(2012, 06, 26), PlatformId = 4 };

            Platform windows = new Platform() { PlatformId = 1, Name = "Windows 10", ShortName = "WIN10", Corporation = "Microsoft", PaidOnlineService = false, Version = 10, NumberOfUsers = 1000000 };
            Platform xBOX = new Platform() { PlatformId = 2, Name = "XBOX Series x", ShortName = "XBOXX", Corporation = "Microsoft", PaidOnlineService = true, Version = 1, NumberOfUsers = 200000 };
            Platform playStation = new Platform() { PlatformId = 3, Name = "Playstation 5", ShortName = "PS5", Corporation = "Sony", PaidOnlineService = true, Version = 5, NumberOfUsers = 210000 };
            Platform @switch = new Platform() { PlatformId = 4, Name = "Nintendo Switch", ShortName = "Switch", Corporation = "Nintendo", PaidOnlineService = true, Version = 1, NumberOfUsers = 120000 };
            Platform android = new Platform() { PlatformId = 5, Name = "Android 11", ShortName = "Android", Corporation = "Google", PaidOnlineService = false, Version = 11, NumberOfUsers = 9500000 };

            modelBuilder?.Entity<Game>(entity =>
            {
                entity.HasOne(game => game.Publisher)
                .WithMany(pub => pub.Games)
                .HasForeignKey(game => game.PublisherId)
                .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(game => game.Platform)
                .WithMany(platform => platform.Games)
                .HasForeignKey(game => game.PlatformId)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Game>().HasData(cyberpunk, leagueOfLegends, witcher3, cSGO, skyrim);
            modelBuilder.Entity<Platform>().HasData(windows, xBOX, playStation, @switch, android);
            modelBuilder.Entity<Publisher>().HasData(cDPrjectRed, riot, bethesda, valve, eA);
         }
    }
}
