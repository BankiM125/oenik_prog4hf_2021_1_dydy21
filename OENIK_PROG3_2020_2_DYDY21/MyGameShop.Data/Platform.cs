﻿// <copyright file="Platform.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Class for platforms.
    /// </summary>
    [Table("platforms")]
    public class Platform
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Platform"/> class.
        /// </summary>
        public Platform()
        {
            this.Games = new HashSet<Game>();
        }

        /// <summary>
        /// Gets or sets the primary key for platforms.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PlatformId { get; set; }

        /// <summary>
        /// Gets or sets the name of platforms.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the short name of platforms.
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Gets or sets the corporation of platforms.
        /// </summary>
        public string Corporation { get; set; }

        /// <summary>
        /// Gets or sets the user number of platforms.
        /// </summary>
        public int NumberOfUsers { get; set; }

        /// <summary>
        /// Gets or sets the version of platforms.
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets the status o f paid online services of platforms.
        /// </summary>
        public bool PaidOnlineService { get; set; }

        /// <summary>
        /// Gets the foreign key of platforms.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Game> Games { get; }
    }
}
