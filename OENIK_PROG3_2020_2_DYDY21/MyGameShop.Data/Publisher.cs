﻿// <copyright file="Publisher.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Class of publishers.
    /// </summary>
    [Table("publishers")]
    public class Publisher
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Publisher"/> class.
        /// </summary>
        public Publisher()
        {
            this.Games = new HashSet<Game>();
        }

        /// <summary>
        /// Gets or sets the primary key of publishers.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PublisherId { get; set; }

        /// <summary>
        /// Gets or sets the name of publishers.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets the triple A qualiry of publishers.
        /// </summary>
        public bool TripleA { get; set; }

        /// <summary>
        /// Gets or sets the country of origin of publishers.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the CEO of publishers.
        /// </summary>
        public string CEO { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets the sale quality of publishers.
        /// </summary>
        public bool DoesSales { get; set; }

        /// <summary>
        /// Gets the foreign key of publishers.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Game> Games { get; }
    }
}
