﻿// <copyright file="Game.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyGameShop.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Class for game entities.
    /// </summary>
    [Table("games")]
    public class Game
    {
        /// <summary>
        ///  Gets or sets the primary key of games.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        ///  Gets or sets the name of games.
        /// </summary>
        [MaxLength(100)]
        [Required]
        public string GameName { get; set; }

        /// <summary>
        ///  Gets or sets the price of games.
        /// </summary>
        public int? Price { get; set; }

        /// <summary>
        ///  Gets or sets the PublisherID of games.
        /// </summary>
        public int PublisherId { get; set; }

        /// <summary>
        ///  Gets or sets the genre of games.
        /// </summary>
        public string Genre { get; set; }

        /// <summary>
        ///  Gets or sets the release date of games.
        /// </summary>
        public DateTime Release { get; set; }

        /// <summary>
        ///  Gets or sets the PlatformID of games.
        /// </summary>
        public int PlatformId { get; set; }

        /// <summary>
        ///  Gets or sets the foreign key for publishers.
        /// </summary>
        [NotMapped]
        [ForeignKey("PublisherId")]
        public virtual Publisher Publisher { get; set; }

        /// <summary>
        ///  Gets or sets the foreign key for platforms.
        /// </summary>
        [NotMapped]
        [ForeignKey("PlatformId")]
        public virtual Platform Platform { get; set; }
    }
}
