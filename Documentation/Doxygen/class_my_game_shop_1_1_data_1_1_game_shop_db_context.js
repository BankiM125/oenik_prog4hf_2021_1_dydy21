var class_my_game_shop_1_1_data_1_1_game_shop_db_context =
[
    [ "GameShopDbContext", "class_my_game_shop_1_1_data_1_1_game_shop_db_context.html#a313f4f091f8414b1698f827df9c00feb", null ],
    [ "GameShopDbContext", "class_my_game_shop_1_1_data_1_1_game_shop_db_context.html#a9816a4c54d5383a90820292a7e8b7f4d", null ],
    [ "OnConfiguring", "class_my_game_shop_1_1_data_1_1_game_shop_db_context.html#a45ab71091f44c3dbb26058cca33a4475", null ],
    [ "OnModelCreating", "class_my_game_shop_1_1_data_1_1_game_shop_db_context.html#a38f961ab08f897e3a8a5aeb8be8e7c1d", null ],
    [ "Games", "class_my_game_shop_1_1_data_1_1_game_shop_db_context.html#a72c2d7e2933ce28b8f373720dead08b9", null ],
    [ "Platforms", "class_my_game_shop_1_1_data_1_1_game_shop_db_context.html#a37d5c307208332ec07351b21be5d981e", null ],
    [ "Publishers", "class_my_game_shop_1_1_data_1_1_game_shop_db_context.html#a6490dc0c3857f409135b2c96da07c3a3", null ]
];