var class_my_game_shop_1_1_repository_1_1_generic_repository =
[
    [ "GenericRepository", "class_my_game_shop_1_1_repository_1_1_generic_repository.html#aa447b0998e888396b8aad178abcd7357", null ],
    [ "Create", "class_my_game_shop_1_1_repository_1_1_generic_repository.html#a7d3aaeecbb42a0757c98a2b32a793938", null ],
    [ "Delete", "class_my_game_shop_1_1_repository_1_1_generic_repository.html#ab76cceedb804c8f1b3d09e24bf56cb4c", null ],
    [ "GetAll", "class_my_game_shop_1_1_repository_1_1_generic_repository.html#ac39382f3ce725792ed081287670c1aa8", null ],
    [ "GetOne", "class_my_game_shop_1_1_repository_1_1_generic_repository.html#a46f2281661282f19b97cc6db31181dfb", null ],
    [ "ctx", "class_my_game_shop_1_1_repository_1_1_generic_repository.html#a5f659c039b1e17676031e3ac7502aeec", null ]
];