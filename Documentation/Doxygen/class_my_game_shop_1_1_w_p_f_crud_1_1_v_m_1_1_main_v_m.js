var class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_main_v_m =
[
    [ "MainVM", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_main_v_m.html#a9c222c68cfa3ab48bd41bdc6eaa84b35", null ],
    [ "MainVM", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_main_v_m.html#afc0a9150f0c5cb012260b2645f61361f", null ],
    [ "AddCmd", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_main_v_m.html#a3a759294ebfba35b7f790af129f4f736", null ],
    [ "DelCmd", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_main_v_m.html#a16c4095c192dfbefd8d46343609cc8fc", null ],
    [ "GetAll", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_main_v_m.html#a4fade2ac313eabcf2c78ce3856fa30d9", null ],
    [ "ModCmd", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_main_v_m.html#a3ef4c3fe0f54a28409348c6d3cc03bef", null ],
    [ "PubSelected", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_main_v_m.html#a52eea8e05902f3217d7b6ea5b1aa91f6", null ],
    [ "Team", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_main_v_m.html#a53a5676dc58ab9d18705b5afa834cd76", null ]
];