var dir_85d2ec3326bab9672b370d9bdadccd9b =
[
    [ "MyGameShop.Data", "dir_5f1f0b84c1c1016c687ede8d5661af81.html", "dir_5f1f0b84c1c1016c687ede8d5661af81" ],
    [ "MyGameShop.Logic", "dir_052e437e41a297ca0c517f55f102bbfd.html", "dir_052e437e41a297ca0c517f55f102bbfd" ],
    [ "MyGameShop.Logic.Tests", "dir_1ca84f85b7d45267751a9b3d276ed133.html", "dir_1ca84f85b7d45267751a9b3d276ed133" ],
    [ "MyGameShop.Program", "dir_c97ccc3cb0fef6bec49383a5af54f65a.html", "dir_c97ccc3cb0fef6bec49383a5af54f65a" ],
    [ "MyGameShop.Repository", "dir_6328215272530d222bde9655ae73f559.html", "dir_6328215272530d222bde9655ae73f559" ],
    [ "MyGameShop.Web", "dir_de8cf400c7dc36b561cdebd9ed7c7319.html", "dir_de8cf400c7dc36b561cdebd9ed7c7319" ],
    [ "MyGameShop.WPFAPI", "dir_8dc02fb1f580fa09c6018aae6ad97b11.html", "dir_8dc02fb1f580fa09c6018aae6ad97b11" ],
    [ "MyGameShop.WPFCrud", "dir_9b0e78ff93003c600d5b69247d2875d1.html", "dir_9b0e78ff93003c600d5b69247d2875d1" ]
];