var class_my_game_shop_1_1_w_p_f_crud_1_1_b_l_1_1_publisher_logic =
[
    [ "PublisherLogic", "class_my_game_shop_1_1_w_p_f_crud_1_1_b_l_1_1_publisher_logic.html#ae25a231758599b1243ec54100f2a51b6", null ],
    [ "AddPublisher", "class_my_game_shop_1_1_w_p_f_crud_1_1_b_l_1_1_publisher_logic.html#a1c2ef16fa68d34e219ef56f64e1eb9d6", null ],
    [ "DelPublisher", "class_my_game_shop_1_1_w_p_f_crud_1_1_b_l_1_1_publisher_logic.html#a9baadc98de148fe8cb2b86afa9674109", null ],
    [ "GetAllPubs", "class_my_game_shop_1_1_w_p_f_crud_1_1_b_l_1_1_publisher_logic.html#afe0d4e079e5a239223f33f286d3c769f", null ],
    [ "ModPublisher", "class_my_game_shop_1_1_w_p_f_crud_1_1_b_l_1_1_publisher_logic.html#a43dd26128019a162a303b1ae23f984a3", null ]
];