var namespace_my_game_shop_1_1_web_1_1_models =
[
    [ "ErrorViewModel", "class_my_game_shop_1_1_web_1_1_models_1_1_error_view_model.html", "class_my_game_shop_1_1_web_1_1_models_1_1_error_view_model" ],
    [ "MapperFactory", "class_my_game_shop_1_1_web_1_1_models_1_1_mapper_factory.html", "class_my_game_shop_1_1_web_1_1_models_1_1_mapper_factory" ],
    [ "Publisher", "class_my_game_shop_1_1_web_1_1_models_1_1_publisher.html", "class_my_game_shop_1_1_web_1_1_models_1_1_publisher" ],
    [ "PubViewModel", "class_my_game_shop_1_1_web_1_1_models_1_1_pub_view_model.html", "class_my_game_shop_1_1_web_1_1_models_1_1_pub_view_model" ]
];