var class_my_game_shop_1_1_data_1_1_game =
[
    [ "GameName", "class_my_game_shop_1_1_data_1_1_game.html#a7995db60e0ab699cf90d51f13750adf9", null ],
    [ "Genre", "class_my_game_shop_1_1_data_1_1_game.html#a4a3e6509f394cb36ad7a93cb2bf56937", null ],
    [ "Id", "class_my_game_shop_1_1_data_1_1_game.html#a34769b9e5156feb61f948531cff1b7c6", null ],
    [ "Platform", "class_my_game_shop_1_1_data_1_1_game.html#a2190c3748070278ab21fc76fd3864f42", null ],
    [ "PlatformId", "class_my_game_shop_1_1_data_1_1_game.html#acda198d9b083afd50a9fe2ff9252dff5", null ],
    [ "Price", "class_my_game_shop_1_1_data_1_1_game.html#a53e756f80dbc55c7c44541434a07b9da", null ],
    [ "Publisher", "class_my_game_shop_1_1_data_1_1_game.html#a3fd568091baa14dbfdc9f474bf2327e4", null ],
    [ "PublisherId", "class_my_game_shop_1_1_data_1_1_game.html#a166f974850109ccec0832deb00f032b8", null ],
    [ "Release", "class_my_game_shop_1_1_data_1_1_game.html#a466f654d9e0e4c112e5d324da475d158", null ]
];