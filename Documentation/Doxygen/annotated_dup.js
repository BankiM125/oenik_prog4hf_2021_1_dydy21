var annotated_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", [
      [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
      [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
      [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
      [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
      [ "Views_Pubs_PubsDetails", "class_asp_net_core_1_1_views___pubs___pubs_details.html", "class_asp_net_core_1_1_views___pubs___pubs_details" ],
      [ "Views_Pubs_PubsEdit", "class_asp_net_core_1_1_views___pubs___pubs_edit.html", "class_asp_net_core_1_1_views___pubs___pubs_edit" ],
      [ "Views_Pubs_PubsIndex", "class_asp_net_core_1_1_views___pubs___pubs_index.html", "class_asp_net_core_1_1_views___pubs___pubs_index" ],
      [ "Views_Pubs_PubsList", "class_asp_net_core_1_1_views___pubs___pubs_list.html", "class_asp_net_core_1_1_views___pubs___pubs_list" ],
      [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
      [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
      [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ]
    ] ],
    [ "MyGameShop", "namespace_my_game_shop.html", [
      [ "Data", "namespace_my_game_shop_1_1_data.html", [
        [ "Game", "class_my_game_shop_1_1_data_1_1_game.html", "class_my_game_shop_1_1_data_1_1_game" ],
        [ "GameShopDbContext", "class_my_game_shop_1_1_data_1_1_game_shop_db_context.html", "class_my_game_shop_1_1_data_1_1_game_shop_db_context" ],
        [ "Platform", "class_my_game_shop_1_1_data_1_1_platform.html", "class_my_game_shop_1_1_data_1_1_platform" ],
        [ "Publisher", "class_my_game_shop_1_1_data_1_1_publisher.html", "class_my_game_shop_1_1_data_1_1_publisher" ]
      ] ],
      [ "Logic", "namespace_my_game_shop_1_1_logic.html", [
        [ "Tests", "namespace_my_game_shop_1_1_logic_1_1_tests.html", [
          [ "NonCRUDTest", "class_my_game_shop_1_1_logic_1_1_tests_1_1_non_c_r_u_d_test.html", "class_my_game_shop_1_1_logic_1_1_tests_1_1_non_c_r_u_d_test" ]
        ] ],
        [ "AdminLogic", "class_my_game_shop_1_1_logic_1_1_admin_logic.html", "class_my_game_shop_1_1_logic_1_1_admin_logic" ],
        [ "Average", "class_my_game_shop_1_1_logic_1_1_average.html", "class_my_game_shop_1_1_logic_1_1_average" ],
        [ "CustomerLogic", "class_my_game_shop_1_1_logic_1_1_customer_logic.html", "class_my_game_shop_1_1_logic_1_1_customer_logic" ],
        [ "GamesOfPubs", "class_my_game_shop_1_1_logic_1_1_games_of_pubs.html", "class_my_game_shop_1_1_logic_1_1_games_of_pubs" ],
        [ "IAdminLogic", "interface_my_game_shop_1_1_logic_1_1_i_admin_logic.html", "interface_my_game_shop_1_1_logic_1_1_i_admin_logic" ],
        [ "ICustomerLogic", "interface_my_game_shop_1_1_logic_1_1_i_customer_logic.html", "interface_my_game_shop_1_1_logic_1_1_i_customer_logic" ],
        [ "PlatformGame", "class_my_game_shop_1_1_logic_1_1_platform_game.html", "class_my_game_shop_1_1_logic_1_1_platform_game" ]
      ] ],
      [ "Repository", "namespace_my_game_shop_1_1_repository.html", [
        [ "GameRepository", "class_my_game_shop_1_1_repository_1_1_game_repository.html", "class_my_game_shop_1_1_repository_1_1_game_repository" ],
        [ "GenericRepository", "class_my_game_shop_1_1_repository_1_1_generic_repository.html", "class_my_game_shop_1_1_repository_1_1_generic_repository" ],
        [ "IGameRepository", "interface_my_game_shop_1_1_repository_1_1_i_game_repository.html", "interface_my_game_shop_1_1_repository_1_1_i_game_repository" ],
        [ "IPlatformRepository", "interface_my_game_shop_1_1_repository_1_1_i_platform_repository.html", "interface_my_game_shop_1_1_repository_1_1_i_platform_repository" ],
        [ "IPublisherRepository", "interface_my_game_shop_1_1_repository_1_1_i_publisher_repository.html", "interface_my_game_shop_1_1_repository_1_1_i_publisher_repository" ],
        [ "IRepository", "interface_my_game_shop_1_1_repository_1_1_i_repository.html", "interface_my_game_shop_1_1_repository_1_1_i_repository" ],
        [ "PlatformRepository", "class_my_game_shop_1_1_repository_1_1_platform_repository.html", "class_my_game_shop_1_1_repository_1_1_platform_repository" ],
        [ "PublisherRepository", "class_my_game_shop_1_1_repository_1_1_publisher_repository.html", "class_my_game_shop_1_1_repository_1_1_publisher_repository" ]
      ] ],
      [ "Web", "namespace_my_game_shop_1_1_web.html", [
        [ "Controllers", "namespace_my_game_shop_1_1_web_1_1_controllers.html", [
          [ "ApiController", "class_my_game_shop_1_1_web_1_1_controllers_1_1_api_controller.html", "class_my_game_shop_1_1_web_1_1_controllers_1_1_api_controller" ],
          [ "ApiResult", "class_my_game_shop_1_1_web_1_1_controllers_1_1_api_result.html", "class_my_game_shop_1_1_web_1_1_controllers_1_1_api_result" ],
          [ "HomeController", "class_my_game_shop_1_1_web_1_1_controllers_1_1_home_controller.html", "class_my_game_shop_1_1_web_1_1_controllers_1_1_home_controller" ],
          [ "PubsController", "class_my_game_shop_1_1_web_1_1_controllers_1_1_pubs_controller.html", "class_my_game_shop_1_1_web_1_1_controllers_1_1_pubs_controller" ]
        ] ],
        [ "Models", "namespace_my_game_shop_1_1_web_1_1_models.html", [
          [ "ErrorViewModel", "class_my_game_shop_1_1_web_1_1_models_1_1_error_view_model.html", "class_my_game_shop_1_1_web_1_1_models_1_1_error_view_model" ],
          [ "MapperFactory", "class_my_game_shop_1_1_web_1_1_models_1_1_mapper_factory.html", "class_my_game_shop_1_1_web_1_1_models_1_1_mapper_factory" ],
          [ "Publisher", "class_my_game_shop_1_1_web_1_1_models_1_1_publisher.html", "class_my_game_shop_1_1_web_1_1_models_1_1_publisher" ],
          [ "PubViewModel", "class_my_game_shop_1_1_web_1_1_models_1_1_pub_view_model.html", "class_my_game_shop_1_1_web_1_1_models_1_1_pub_view_model" ]
        ] ],
        [ "Factory", "class_my_game_shop_1_1_web_1_1_factory.html", "class_my_game_shop_1_1_web_1_1_factory" ],
        [ "Program", "class_my_game_shop_1_1_web_1_1_program.html", "class_my_game_shop_1_1_web_1_1_program" ],
        [ "Startup", "class_my_game_shop_1_1_web_1_1_startup.html", "class_my_game_shop_1_1_web_1_1_startup" ]
      ] ],
      [ "WPFAPI", "namespace_my_game_shop_1_1_w_p_f_a_p_i.html", [
        [ "BL", "namespace_my_game_shop_1_1_w_p_f_a_p_i_1_1_b_l.html", [
          [ "MainLogic", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_b_l_1_1_main_logic.html", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_b_l_1_1_main_logic" ]
        ] ],
        [ "VM", "namespace_my_game_shop_1_1_w_p_f_a_p_i_1_1_v_m.html", [
          [ "MainVM", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_v_m_1_1_main_v_m.html", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_v_m_1_1_main_v_m" ],
          [ "PubVM", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_v_m_1_1_pub_v_m.html", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_v_m_1_1_pub_v_m" ]
        ] ],
        [ "App", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_app.html", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_app" ],
        [ "EditorWindow", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_editor_window.html", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_editor_window" ],
        [ "MainWindow", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_main_window.html", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_main_window" ]
      ] ],
      [ "WPFCrud", "namespace_my_game_shop_1_1_w_p_f_crud.html", [
        [ "BL", "namespace_my_game_shop_1_1_w_p_f_crud_1_1_b_l.html", [
          [ "IPublisherLogic", "interface_my_game_shop_1_1_w_p_f_crud_1_1_b_l_1_1_i_publisher_logic.html", "interface_my_game_shop_1_1_w_p_f_crud_1_1_b_l_1_1_i_publisher_logic" ],
          [ "PublisherLogic", "class_my_game_shop_1_1_w_p_f_crud_1_1_b_l_1_1_publisher_logic.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_b_l_1_1_publisher_logic" ]
        ] ],
        [ "Data", "namespace_my_game_shop_1_1_w_p_f_crud_1_1_data.html", [
          [ "WPFPublisher", "class_my_game_shop_1_1_w_p_f_crud_1_1_data_1_1_w_p_f_publisher.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_data_1_1_w_p_f_publisher" ]
        ] ],
        [ "UI", "namespace_my_game_shop_1_1_w_p_f_crud_1_1_u_i.html", [
          [ "EditorServiceViaWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_u_i_1_1_editor_service_via_window.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_u_i_1_1_editor_service_via_window" ]
        ] ],
        [ "VM", "namespace_my_game_shop_1_1_w_p_f_crud_1_1_v_m.html", [
          [ "EditorVM", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_editor_v_m.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_editor_v_m" ],
          [ "MainVM", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_main_v_m.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_main_v_m" ]
        ] ],
        [ "App", "class_my_game_shop_1_1_w_p_f_crud_1_1_app.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_app" ],
        [ "IEditorService", "interface_my_game_shop_1_1_w_p_f_crud_1_1_i_editor_service.html", "interface_my_game_shop_1_1_w_p_f_crud_1_1_i_editor_service" ],
        [ "EditorWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window" ],
        [ "Factory", "class_my_game_shop_1_1_w_p_f_crud_1_1_factory.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_factory" ],
        [ "MainWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window" ],
        [ "MyIOC", "class_my_game_shop_1_1_w_p_f_crud_1_1_my_i_o_c.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_my_i_o_c" ]
      ] ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ],
    [ "Program", "class_program.html", null ]
];