var namespace_my_game_shop_1_1_logic =
[
    [ "Tests", "namespace_my_game_shop_1_1_logic_1_1_tests.html", "namespace_my_game_shop_1_1_logic_1_1_tests" ],
    [ "AdminLogic", "class_my_game_shop_1_1_logic_1_1_admin_logic.html", "class_my_game_shop_1_1_logic_1_1_admin_logic" ],
    [ "Average", "class_my_game_shop_1_1_logic_1_1_average.html", "class_my_game_shop_1_1_logic_1_1_average" ],
    [ "CustomerLogic", "class_my_game_shop_1_1_logic_1_1_customer_logic.html", "class_my_game_shop_1_1_logic_1_1_customer_logic" ],
    [ "GamesOfPubs", "class_my_game_shop_1_1_logic_1_1_games_of_pubs.html", "class_my_game_shop_1_1_logic_1_1_games_of_pubs" ],
    [ "IAdminLogic", "interface_my_game_shop_1_1_logic_1_1_i_admin_logic.html", "interface_my_game_shop_1_1_logic_1_1_i_admin_logic" ],
    [ "ICustomerLogic", "interface_my_game_shop_1_1_logic_1_1_i_customer_logic.html", "interface_my_game_shop_1_1_logic_1_1_i_customer_logic" ],
    [ "PlatformGame", "class_my_game_shop_1_1_logic_1_1_platform_game.html", "class_my_game_shop_1_1_logic_1_1_platform_game" ]
];