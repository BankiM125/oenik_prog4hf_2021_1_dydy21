var class_my_game_shop_1_1_web_1_1_factory =
[
    [ "Factory", "class_my_game_shop_1_1_web_1_1_factory.html#abda3587ebbdd4bb5955870817ad3549c", null ],
    [ "Ctx", "class_my_game_shop_1_1_web_1_1_factory.html#a240e3a2880055de971bbc32607b91e36", null ],
    [ "GameLogic", "class_my_game_shop_1_1_web_1_1_factory.html#a4035b081a1de434bd0383bbb11875273", null ],
    [ "GameRepo", "class_my_game_shop_1_1_web_1_1_factory.html#a26df641257d038c6a47b8ac5b88d0c49", null ],
    [ "PlatRepo", "class_my_game_shop_1_1_web_1_1_factory.html#a37fc428a186817d6e86a24d7f1771147", null ],
    [ "PubCustLogic", "class_my_game_shop_1_1_web_1_1_factory.html#acfdc2dc1044d60aa3fdd64293a4ab195", null ],
    [ "PubLogic", "class_my_game_shop_1_1_web_1_1_factory.html#ad0201b11b6a528db142f76a49c15ea59", null ],
    [ "PubRepo", "class_my_game_shop_1_1_web_1_1_factory.html#a1e49026004c0ad949b5a8db48b99c1da", null ]
];