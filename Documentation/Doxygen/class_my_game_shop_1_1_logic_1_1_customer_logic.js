var class_my_game_shop_1_1_logic_1_1_customer_logic =
[
    [ "CustomerLogic", "class_my_game_shop_1_1_logic_1_1_customer_logic.html#a722909f92fdbf3012e23cee42fb8bbd7", null ],
    [ "AverageOnPlatform", "class_my_game_shop_1_1_logic_1_1_customer_logic.html#ae3a676c38b89b3feadeade6d341df45d", null ],
    [ "AverageOnPlatformAsync", "class_my_game_shop_1_1_logic_1_1_customer_logic.html#a51137b47a3670e6196c42f7a4fdf9976", null ],
    [ "GamesOnPlatform", "class_my_game_shop_1_1_logic_1_1_customer_logic.html#ad962f4378d58b0be1798c5d2c905ddee", null ],
    [ "GamesOnPlatformAsync", "class_my_game_shop_1_1_logic_1_1_customer_logic.html#aee67b937d126b0457ce35bc3647823d4", null ],
    [ "GetAll", "class_my_game_shop_1_1_logic_1_1_customer_logic.html#ab372ec748e7946828171a03e088e76cc", null ],
    [ "GetGameGenres", "class_my_game_shop_1_1_logic_1_1_customer_logic.html#a47ad8a6e47937b962359422d5f9eff74", null ],
    [ "GetGenresOfPublishers", "class_my_game_shop_1_1_logic_1_1_customer_logic.html#a796289938d5fce6079878abb80fb295d", null ],
    [ "GetGenresOfPublishersAsync", "class_my_game_shop_1_1_logic_1_1_customer_logic.html#af040e40433b3be8e7b651ad2ffd12bc9", null ],
    [ "GetOne", "class_my_game_shop_1_1_logic_1_1_customer_logic.html#a1bafa9707fc0521848ed0a9ea0ab36e8", null ]
];