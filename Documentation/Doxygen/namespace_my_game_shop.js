var namespace_my_game_shop =
[
    [ "Data", "namespace_my_game_shop_1_1_data.html", "namespace_my_game_shop_1_1_data" ],
    [ "Logic", "namespace_my_game_shop_1_1_logic.html", "namespace_my_game_shop_1_1_logic" ],
    [ "Repository", "namespace_my_game_shop_1_1_repository.html", "namespace_my_game_shop_1_1_repository" ],
    [ "Web", "namespace_my_game_shop_1_1_web.html", "namespace_my_game_shop_1_1_web" ],
    [ "WPFAPI", "namespace_my_game_shop_1_1_w_p_f_a_p_i.html", "namespace_my_game_shop_1_1_w_p_f_a_p_i" ],
    [ "WPFCrud", "namespace_my_game_shop_1_1_w_p_f_crud.html", "namespace_my_game_shop_1_1_w_p_f_crud" ]
];