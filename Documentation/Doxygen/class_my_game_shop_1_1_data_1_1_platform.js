var class_my_game_shop_1_1_data_1_1_platform =
[
    [ "Platform", "class_my_game_shop_1_1_data_1_1_platform.html#a100e1da04daa56abaa36b461aab31fcc", null ],
    [ "Corporation", "class_my_game_shop_1_1_data_1_1_platform.html#a672fa593a5d37e8ef59c7bb46a20819c", null ],
    [ "Games", "class_my_game_shop_1_1_data_1_1_platform.html#acc9ad8e806dc7d21e8a14e49cf28a206", null ],
    [ "Name", "class_my_game_shop_1_1_data_1_1_platform.html#a2645324fce63635a4e3ee4e0b9ee4f57", null ],
    [ "NumberOfUsers", "class_my_game_shop_1_1_data_1_1_platform.html#af3559ac11531cdffc1aedd64c2b5d4c6", null ],
    [ "PaidOnlineService", "class_my_game_shop_1_1_data_1_1_platform.html#ae63df30a528caae1b660f75544b85d57", null ],
    [ "PlatformId", "class_my_game_shop_1_1_data_1_1_platform.html#a362ec607c6f657b532cfcf015df7bfe7", null ],
    [ "ShortName", "class_my_game_shop_1_1_data_1_1_platform.html#ab688a39731663d54568cde606f36e831", null ],
    [ "Version", "class_my_game_shop_1_1_data_1_1_platform.html#ab02125e5b439eddafcd555cd69163df6", null ]
];