var class_my_game_shop_1_1_data_1_1_publisher =
[
    [ "Publisher", "class_my_game_shop_1_1_data_1_1_publisher.html#a4be3773da6caa12c6755160e1fdbebee", null ],
    [ "CEO", "class_my_game_shop_1_1_data_1_1_publisher.html#a8ab6b89fccf424b14bc013dac55863db", null ],
    [ "Country", "class_my_game_shop_1_1_data_1_1_publisher.html#a853fac2fbb19d248d480bf0411f8d748", null ],
    [ "DoesSales", "class_my_game_shop_1_1_data_1_1_publisher.html#ad45fa228c066b957213a631d9f94412e", null ],
    [ "Games", "class_my_game_shop_1_1_data_1_1_publisher.html#a3d2c70ca5805674afd554617130bf1ec", null ],
    [ "Name", "class_my_game_shop_1_1_data_1_1_publisher.html#a4d73408bb50ea7eb8156885770db9031", null ],
    [ "PublisherId", "class_my_game_shop_1_1_data_1_1_publisher.html#a9d803dfcf22d8800bea0d02892b3b6be", null ],
    [ "TripleA", "class_my_game_shop_1_1_data_1_1_publisher.html#af3de2f3765c531ed7dc29c38126dc3c6", null ]
];