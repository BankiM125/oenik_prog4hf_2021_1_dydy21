var class_my_game_shop_1_1_logic_1_1_admin_logic =
[
    [ "AdminLogic", "class_my_game_shop_1_1_logic_1_1_admin_logic.html#adca338b79fd19a24ed990957a029a6f8", null ],
    [ "Create", "class_my_game_shop_1_1_logic_1_1_admin_logic.html#a0a342b5e758c38a8a41a6cdf9d083407", null ],
    [ "Create", "class_my_game_shop_1_1_logic_1_1_admin_logic.html#af392b07e21514df0117f862515688ae3", null ],
    [ "Create", "class_my_game_shop_1_1_logic_1_1_admin_logic.html#a4186b7cfe00d7958647957aa0ae45234", null ],
    [ "Remove", "class_my_game_shop_1_1_logic_1_1_admin_logic.html#a17438baa8754a8febb1b4bfdfe1f8909", null ],
    [ "Remove", "class_my_game_shop_1_1_logic_1_1_admin_logic.html#a703ec1aa4ae443fe3df45b884e454897", null ],
    [ "Remove", "class_my_game_shop_1_1_logic_1_1_admin_logic.html#a8330ce352ffaf93ad50683e44f5915f3", null ],
    [ "Update", "class_my_game_shop_1_1_logic_1_1_admin_logic.html#ae42d780a39df99f04162916107b23f82", null ],
    [ "Update", "class_my_game_shop_1_1_logic_1_1_admin_logic.html#a89acac8005fd7a99a129d5d82c8eebc9", null ],
    [ "Update", "class_my_game_shop_1_1_logic_1_1_admin_logic.html#ac795b5f5eb702cd1dac31111bb7934ba", null ]
];