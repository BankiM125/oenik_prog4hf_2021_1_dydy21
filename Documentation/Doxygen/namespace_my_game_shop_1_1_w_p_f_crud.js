var namespace_my_game_shop_1_1_w_p_f_crud =
[
    [ "BL", "namespace_my_game_shop_1_1_w_p_f_crud_1_1_b_l.html", "namespace_my_game_shop_1_1_w_p_f_crud_1_1_b_l" ],
    [ "Data", "namespace_my_game_shop_1_1_w_p_f_crud_1_1_data.html", "namespace_my_game_shop_1_1_w_p_f_crud_1_1_data" ],
    [ "UI", "namespace_my_game_shop_1_1_w_p_f_crud_1_1_u_i.html", "namespace_my_game_shop_1_1_w_p_f_crud_1_1_u_i" ],
    [ "VM", "namespace_my_game_shop_1_1_w_p_f_crud_1_1_v_m.html", "namespace_my_game_shop_1_1_w_p_f_crud_1_1_v_m" ],
    [ "App", "class_my_game_shop_1_1_w_p_f_crud_1_1_app.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_app" ],
    [ "IEditorService", "interface_my_game_shop_1_1_w_p_f_crud_1_1_i_editor_service.html", "interface_my_game_shop_1_1_w_p_f_crud_1_1_i_editor_service" ],
    [ "EditorWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window" ],
    [ "Factory", "class_my_game_shop_1_1_w_p_f_crud_1_1_factory.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_factory" ],
    [ "MainWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window" ],
    [ "MyIOC", "class_my_game_shop_1_1_w_p_f_crud_1_1_my_i_o_c.html", "class_my_game_shop_1_1_w_p_f_crud_1_1_my_i_o_c" ]
];