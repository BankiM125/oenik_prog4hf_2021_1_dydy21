var namespace_my_game_shop_1_1_repository =
[
    [ "GameRepository", "class_my_game_shop_1_1_repository_1_1_game_repository.html", "class_my_game_shop_1_1_repository_1_1_game_repository" ],
    [ "GenericRepository", "class_my_game_shop_1_1_repository_1_1_generic_repository.html", "class_my_game_shop_1_1_repository_1_1_generic_repository" ],
    [ "IGameRepository", "interface_my_game_shop_1_1_repository_1_1_i_game_repository.html", "interface_my_game_shop_1_1_repository_1_1_i_game_repository" ],
    [ "IPlatformRepository", "interface_my_game_shop_1_1_repository_1_1_i_platform_repository.html", "interface_my_game_shop_1_1_repository_1_1_i_platform_repository" ],
    [ "IPublisherRepository", "interface_my_game_shop_1_1_repository_1_1_i_publisher_repository.html", "interface_my_game_shop_1_1_repository_1_1_i_publisher_repository" ],
    [ "IRepository", "interface_my_game_shop_1_1_repository_1_1_i_repository.html", "interface_my_game_shop_1_1_repository_1_1_i_repository" ],
    [ "PlatformRepository", "class_my_game_shop_1_1_repository_1_1_platform_repository.html", "class_my_game_shop_1_1_repository_1_1_platform_repository" ],
    [ "PublisherRepository", "class_my_game_shop_1_1_repository_1_1_publisher_repository.html", "class_my_game_shop_1_1_repository_1_1_publisher_repository" ]
];