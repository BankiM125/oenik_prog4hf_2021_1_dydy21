var hierarchy =
[
    [ "MyGameShop.Web.Controllers.ApiResult", "class_my_game_shop_1_1_web_1_1_controllers_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "MyGameShop.WPFAPI.App", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "MyGameShop.WPFAPI.App", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_app.html", null ],
      [ "MyGameShop.WPFAPI.App", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_app.html", null ],
      [ "MyGameShop.WPFCrud.App", "class_my_game_shop_1_1_w_p_f_crud_1_1_app.html", null ],
      [ "MyGameShop.WPFCrud.App", "class_my_game_shop_1_1_w_p_f_crud_1_1_app.html", null ],
      [ "MyGameShop.WPFCrud.App", "class_my_game_shop_1_1_w_p_f_crud_1_1_app.html", null ],
      [ "MyGameShop.WPFCrud.App", "class_my_game_shop_1_1_w_p_f_crud_1_1_app.html", null ],
      [ "MyGameShop.WPFCrud.App", "class_my_game_shop_1_1_w_p_f_crud_1_1_app.html", null ]
    ] ],
    [ "MyGameShop.Logic.Average", "class_my_game_shop_1_1_logic_1_1_average.html", null ],
    [ "Controller", null, [
      [ "MyGameShop.Web.Controllers.ApiController", "class_my_game_shop_1_1_web_1_1_controllers_1_1_api_controller.html", null ],
      [ "MyGameShop.Web.Controllers.HomeController", "class_my_game_shop_1_1_web_1_1_controllers_1_1_home_controller.html", null ],
      [ "MyGameShop.Web.Controllers.PubsController", "class_my_game_shop_1_1_web_1_1_controllers_1_1_pubs_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "MyGameShop.Data.GameShopDbContext", "class_my_game_shop_1_1_data_1_1_game_shop_db_context.html", null ]
    ] ],
    [ "MyGameShop.Web.Models.ErrorViewModel", "class_my_game_shop_1_1_web_1_1_models_1_1_error_view_model.html", null ],
    [ "MyGameShop.Web.Factory", "class_my_game_shop_1_1_web_1_1_factory.html", null ],
    [ "MyGameShop.WPFCrud.Factory", "class_my_game_shop_1_1_w_p_f_crud_1_1_factory.html", null ],
    [ "MyGameShop.Data.Game", "class_my_game_shop_1_1_data_1_1_game.html", null ],
    [ "MyGameShop.Logic.GamesOfPubs", "class_my_game_shop_1_1_logic_1_1_games_of_pubs.html", null ],
    [ "MyGameShop.Repository.GenericRepository< Game >", "class_my_game_shop_1_1_repository_1_1_generic_repository.html", [
      [ "MyGameShop.Repository.GameRepository", "class_my_game_shop_1_1_repository_1_1_game_repository.html", null ]
    ] ],
    [ "MyGameShop.Repository.GenericRepository< Platform >", "class_my_game_shop_1_1_repository_1_1_generic_repository.html", [
      [ "MyGameShop.Repository.PlatformRepository", "class_my_game_shop_1_1_repository_1_1_platform_repository.html", null ]
    ] ],
    [ "MyGameShop.Repository.GenericRepository< Publisher >", "class_my_game_shop_1_1_repository_1_1_generic_repository.html", [
      [ "MyGameShop.Repository.PublisherRepository", "class_my_game_shop_1_1_repository_1_1_publisher_repository.html", null ]
    ] ],
    [ "MyGameShop.Logic.IAdminLogic< Game >", "interface_my_game_shop_1_1_logic_1_1_i_admin_logic.html", [
      [ "MyGameShop.Logic.AdminLogic", "class_my_game_shop_1_1_logic_1_1_admin_logic.html", null ]
    ] ],
    [ "MyGameShop.Logic.IAdminLogic< Platform >", "interface_my_game_shop_1_1_logic_1_1_i_admin_logic.html", [
      [ "MyGameShop.Logic.AdminLogic", "class_my_game_shop_1_1_logic_1_1_admin_logic.html", null ]
    ] ],
    [ "MyGameShop.Logic.IAdminLogic< Publisher >", "interface_my_game_shop_1_1_logic_1_1_i_admin_logic.html", [
      [ "MyGameShop.Logic.AdminLogic", "class_my_game_shop_1_1_logic_1_1_admin_logic.html", null ]
    ] ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "MyGameShop.WPFAPI.EditorWindow", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFAPI.EditorWindow", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFAPI.MainWindow", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_main_window.html", null ],
      [ "MyGameShop.WPFAPI.MainWindow", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_main_window.html", null ],
      [ "MyGameShop.WPFCrud.EditorWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFCrud.EditorWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFCrud.EditorWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFCrud.EditorWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFCrud.MainWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window.html", null ],
      [ "MyGameShop.WPFCrud.MainWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window.html", null ],
      [ "MyGameShop.WPFCrud.MainWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window.html", null ],
      [ "MyGameShop.WPFCrud.MainWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window.html", null ]
    ] ],
    [ "MyGameShop.Logic.ICustomerLogic< T >", "interface_my_game_shop_1_1_logic_1_1_i_customer_logic.html", null ],
    [ "MyGameShop.Logic.ICustomerLogic< Game >", "interface_my_game_shop_1_1_logic_1_1_i_customer_logic.html", [
      [ "MyGameShop.Logic.CustomerLogic", "class_my_game_shop_1_1_logic_1_1_customer_logic.html", [
        [ "MyGameShop.Logic.AdminLogic", "class_my_game_shop_1_1_logic_1_1_admin_logic.html", null ]
      ] ],
      [ "MyGameShop.Logic.IAdminLogic< T >", "interface_my_game_shop_1_1_logic_1_1_i_admin_logic.html", null ]
    ] ],
    [ "MyGameShop.Logic.ICustomerLogic< Platform >", "interface_my_game_shop_1_1_logic_1_1_i_customer_logic.html", [
      [ "MyGameShop.Logic.CustomerLogic", "class_my_game_shop_1_1_logic_1_1_customer_logic.html", null ],
      [ "MyGameShop.Logic.IAdminLogic< T >", "interface_my_game_shop_1_1_logic_1_1_i_admin_logic.html", null ]
    ] ],
    [ "MyGameShop.Logic.ICustomerLogic< Publisher >", "interface_my_game_shop_1_1_logic_1_1_i_customer_logic.html", [
      [ "MyGameShop.Logic.CustomerLogic", "class_my_game_shop_1_1_logic_1_1_customer_logic.html", null ],
      [ "MyGameShop.Logic.IAdminLogic< T >", "interface_my_game_shop_1_1_logic_1_1_i_admin_logic.html", null ]
    ] ],
    [ "MyGameShop.WPFCrud.IEditorService", "interface_my_game_shop_1_1_w_p_f_crud_1_1_i_editor_service.html", [
      [ "MyGameShop.WPFCrud.UI.EditorServiceViaWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "MyGameShop.WPFCrud.BL.IPublisherLogic", "interface_my_game_shop_1_1_w_p_f_crud_1_1_b_l_1_1_i_publisher_logic.html", [
      [ "MyGameShop.WPFCrud.BL.PublisherLogic", "class_my_game_shop_1_1_w_p_f_crud_1_1_b_l_1_1_publisher_logic.html", null ]
    ] ],
    [ "MyGameShop.Repository.IRepository< T >", "interface_my_game_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyGameShop.Repository.GenericRepository< T >", "class_my_game_shop_1_1_repository_1_1_generic_repository.html", null ]
    ] ],
    [ "MyGameShop.Repository.IRepository< Game >", "interface_my_game_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyGameShop.Repository.IGameRepository", "interface_my_game_shop_1_1_repository_1_1_i_game_repository.html", [
        [ "MyGameShop.Repository.GameRepository", "class_my_game_shop_1_1_repository_1_1_game_repository.html", null ]
      ] ]
    ] ],
    [ "MyGameShop.Repository.IRepository< Platform >", "interface_my_game_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyGameShop.Repository.IPlatformRepository", "interface_my_game_shop_1_1_repository_1_1_i_platform_repository.html", [
        [ "MyGameShop.Repository.PlatformRepository", "class_my_game_shop_1_1_repository_1_1_platform_repository.html", null ]
      ] ]
    ] ],
    [ "MyGameShop.Repository.IRepository< Publisher >", "interface_my_game_shop_1_1_repository_1_1_i_repository.html", [
      [ "MyGameShop.Repository.IPublisherRepository", "interface_my_game_shop_1_1_repository_1_1_i_publisher_repository.html", [
        [ "MyGameShop.Repository.PublisherRepository", "class_my_game_shop_1_1_repository_1_1_publisher_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "MyGameShop.WPFCrud.MyIOC", "class_my_game_shop_1_1_w_p_f_crud_1_1_my_i_o_c.html", null ]
    ] ],
    [ "MyGameShop.WPFAPI.BL.MainLogic", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_b_l_1_1_main_logic.html", null ],
    [ "MyGameShop.Web.Models.MapperFactory", "class_my_game_shop_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "MyGameShop.Logic.Tests.NonCRUDTest", "class_my_game_shop_1_1_logic_1_1_tests_1_1_non_c_r_u_d_test.html", null ],
    [ "ObservableObject", null, [
      [ "MyGameShop.WPFAPI.VM.PubVM", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_v_m_1_1_pub_v_m.html", null ],
      [ "MyGameShop.WPFCrud.Data.WPFPublisher", "class_my_game_shop_1_1_w_p_f_crud_1_1_data_1_1_w_p_f_publisher.html", null ]
    ] ],
    [ "MyGameShop.Data.Platform", "class_my_game_shop_1_1_data_1_1_platform.html", null ],
    [ "MyGameShop.Logic.PlatformGame", "class_my_game_shop_1_1_logic_1_1_platform_game.html", null ],
    [ "MyGameShop.Web.Program", "class_my_game_shop_1_1_web_1_1_program.html", null ],
    [ "Program", "class_program.html", null ],
    [ "MyGameShop.Data.Publisher", "class_my_game_shop_1_1_data_1_1_publisher.html", null ],
    [ "MyGameShop.Web.Models.Publisher", "class_my_game_shop_1_1_web_1_1_models_1_1_publisher.html", null ],
    [ "MyGameShop.Web.Models.PubViewModel", "class_my_game_shop_1_1_web_1_1_models_1_1_pub_view_model.html", null ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage", null, [
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Pubs_PubsDetails", "class_asp_net_core_1_1_views___pubs___pubs_details.html", null ],
      [ "AspNetCore.Views_Pubs_PubsEdit", "class_asp_net_core_1_1_views___pubs___pubs_edit.html", null ],
      [ "AspNetCore.Views_Pubs_PubsIndex", "class_asp_net_core_1_1_views___pubs___pubs_index.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< MyGameShop.Web.Models.Publisher >>", null, [
      [ "AspNetCore.Views_Pubs_PubsList", "class_asp_net_core_1_1_views___pubs___pubs_list.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "MyGameShop.WPFCrud.MyIOC", "class_my_game_shop_1_1_w_p_f_crud_1_1_my_i_o_c.html", null ]
    ] ],
    [ "MyGameShop.Web.Startup", "class_my_game_shop_1_1_web_1_1_startup.html", null ],
    [ "ViewModelBase", null, [
      [ "MyGameShop.WPFAPI.VM.MainVM", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_v_m_1_1_main_v_m.html", null ],
      [ "MyGameShop.WPFCrud.VM.EditorVM", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_editor_v_m.html", null ],
      [ "MyGameShop.WPFCrud.VM.MainVM", "class_my_game_shop_1_1_w_p_f_crud_1_1_v_m_1_1_main_v_m.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "MyGameShop.WPFAPI.EditorWindow", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFAPI.EditorWindow", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFAPI.MainWindow", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_main_window.html", null ],
      [ "MyGameShop.WPFAPI.MainWindow", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_main_window.html", null ],
      [ "MyGameShop.WPFAPI.MainWindow", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_main_window.html", null ],
      [ "MyGameShop.WPFCrud.EditorWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFCrud.EditorWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFCrud.EditorWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFCrud.EditorWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFCrud.EditorWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_editor_window.html", null ],
      [ "MyGameShop.WPFCrud.MainWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window.html", null ],
      [ "MyGameShop.WPFCrud.MainWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window.html", null ],
      [ "MyGameShop.WPFCrud.MainWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window.html", null ],
      [ "MyGameShop.WPFCrud.MainWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window.html", null ],
      [ "MyGameShop.WPFCrud.MainWindow", "class_my_game_shop_1_1_w_p_f_crud_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "MyGameShop.WPFAPI.EditorWindow", "class_my_game_shop_1_1_w_p_f_a_p_i_1_1_editor_window.html", null ]
    ] ]
];